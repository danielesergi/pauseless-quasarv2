# PauselessApp (pauseless-quasarv2)

Pauseless app 2021

## Install the dependencies
```bash
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```

### Lint the files
```bash
npm run lint
```

### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).

### scelte iniziali quasar CLI

? Project name (internal usage for dev) pauseless-quasarv2
? Project product name (must start with letter if building mobile apps) PauselessApp
? Project description Pauseless app 2021
? Author Daniele Sergi <daniele.sergi@plurimedia.it>
? Pick your CSS preprocessor: SCSS
? Check the features needed for your project: ESLint (recommended), Axios
? Pick an ESLint preset: Standard
? Continue to install project dependencies after the project has been created? (recommended) NPM

### testing framework
```bash
quasar ext add @quasar/testing
```
ho selezionato jest e 

Overwrite babel.config.js and use additional .babelrc 
SI

poi : 
Extra "scripts" in your package.json
SI

Install Wallaby.js (integrazione con editor di codice)
SI

(ho installato l'estensione di wallaby per vscode)

Extra scripts in your package.json
SI

```bash
quasar test --e2e cypress
quasar test --e2e jest
```

ATTENZIONE : jest per ora non funziona (messaggio in forum quasar del 18 febbraio) e di conseguenza nemmeno wallaby, uso solo cypress

### come lanciare i test e2e

lanciare il server da VScode in modalità TEST
npm run test:e2e

### come lanciare i test unit

Ho copiato il metodo dei test delle mie vecchie app, installando
gulp chai gulp-mocha chai-http
ho aggiunto il gulpfile
npm run test:unit

ho diviso i test in 3 cartelle : 
1.preliminari
testo le librerie, in questo caso solo quella che legge l'xml
2.routes
testo le singole rotte server senza istanziare l'applicazione
3.api
testo le rotte istanziando l'applicazione con utente test (sono ancora tutti da fare)

### installazione cordova e android

installare android studio e dalla prima schermata configurare AVD manager scaricando qualche immagine e sdk manager scaricando qualche versione di android compresa l'ultima
creare una AVD per il deploy dell'app
installare gradle (brew install gradle)
installare cordova (npm install -g cordova)
creare un progetto di test (cordova create reqTest ; cd reqTest ; cordova requirements) per controllare che tutto sia a posto
cancellare il progetto di cui sopra
entrare nella cartella del progetto quasar
cordova platform add android
lanciare quasar build -m cordova -T android
cd src-cordova ; cordova requirements android
se tutto è a posto si può lanciare quasar dev -m cordova -T android
io ho avuto bisogno, per il primo lancio su AVD di fare quasar dev -m android --ide
dentro la cartella src-cordova possiamo installare dei plugin cordova plugin add cordova-plugin-file


passi fatti stavolta : 
lanciare quasar build -m cordova -T android
cd src-cordova
cordova platform add android
cordova requirements android (per controllare)
quasar dev -m android --ide (installato gradle wrapper, aggiornato alla 6.1.1 in wrapper.properties)
quasar dev -m cordova -T android

altri passi da capo
quasar mode add cordova
cd src-cordova
cordova platform add android
cordova requirements (controllare)
cd ..
quasar dev -m android --ide
cd src-cordova
cordova plugin add cordova-plugin-file
cordova plugin add cordova-plugin-nativestorage

RICORDA : se cambi versione di node, devi reinstallare cordova!!

### comandi quasar utili
quasar new store uscite
quasar new page Avanzate
quasar new boot vue-draggable

### lancio con Cordova

sperando di aver configurato tutto bene prima, basta
quasar dev -m cordova -T android
poi per ispezionare i messaggi di log del frontend, su chrome : 
chrome://inspect/#devices
e cliccare inspect sulla device (l'AVD android o il telefono collegato)

### PER TERMINARE L'EMULATORE ANDROID
killall -9 qemu-system-x86_64

### lancio su device reale

abilitare le opzioni sviluppatore andando in opzioni, info telefono, clicare 7 volte sul numero della build, poi tornare in opzioni, opzioni sviluppatore, enable debig USB
connettere il telefono e lanciare 
quasar dev -m cordova -T android --device

### release mobile

(sdk use java 8.0.352-tem)
quasar build -m cordova -T android
(da errore il css minimizer :O )
per risolvere l'errore : la mia attuale versione di @quasar/app usa una versione di yaml che ha dei nomi nella cartella dist diversi da quelli che si aspetta. Per rimediare ho dovuto rinominare, nella cartella node_modules/yaml/dist i files : 
Document
resolveSeq
Schema
warnings
non c'è stato bisogno di rinominare PlainValue, ma potrebbe esserci in futuro.
./create-dist.sh

(short)

source ~/.bashrc
nvm use
sdk use java 8.0.352-tem
quasar build -m cordova -T android
./create-dist.sh

## funzionamento

### mix temporanei
l'app mobile invia al server (ip nei settings) un mix, che viene salvato in uno strano formato xml/json nella cartella dell'utente
l'app pc vede il mix nei mix temporanei
cliccando salva mix, il mix viene aggiunto alle variabili in memoria del server. Viene cancellato il file temporaneo e viene settato "unsaved" sull'app pc.
al click sull'icona di salvataggio rossa i mix in memoria vengono dumpati nel file xml dei mix

### appunti

registrare globalmente un componente https://www.youtube.com/watch?v=zOAcCVTUIQY&ab_channel=LukeDiebold
esempi draggable https://github.com/SortableJS/Vue.Draggable/blob/master/example/components/simple.vue

### gestione suoni

Ho rinominato gli mp3 # sostituendo nel nome il carattere # con _ altrimenti dava errore nel caricamento (lo prendeva come l'hash di un url)