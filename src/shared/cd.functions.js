import { api } from 'boot/axios'
import { inject } from 'vue'
import _cloneDeep from 'lodash/cloneDeep'

const aspetta = async (s) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve()
    }, s * 1000)
  })
}
export default function () {
  // const blankMix = { $: { name: 'current mix', description: '', bpm: '000' }, song: [] }
  const $store = inject('store')
  const loadCdFile = async function () {
    // mi pare che questa funzione venga richiamata solo in caso non siamo su mobile, per cui il caricamento dei file json è inutile
    // $store.commit('application/addLog', 'funzione loadCdFile')
    /* if (Platform.is.cordova && Platform.is.mobile) {
      $store.commit('application/addLog', 'siamo su mobile')
      fileService.read('json_data', 'base_cd_list.json')
        .then(
          (a) => {
            // console.log(a)
            console.log('caricato cd file', JSON.parse(a))
            $store.commit('application/addLog', 'caricato cd file')
            return a
          },
          (e) => {
            fileService.list().then(
              (r) => {
                $store.commit('application/addLog', JSON.stringify(r))
                console.log('chiavi presenti', JSON.stringify(r))
                console.error('errore get file', e, JSON.stringify(r))
              },
              (e) => {
                console.error('errore get keys', e)
                console.error('errore get file', e)
              }
            )
            console.log('errore caricamento', e)
          }
        )
    } else { */
    try {
      // per i test asincroni
      // await aspetta(5)
      const response = await api.get('json/cd')
      return response.data
    } catch (e) {
      console.error('errore get file cd (web)', e)
    }
    // }
  }

  const getCd = function (name, cdList) {
    try {
      for (const cd of cdList || $store.gCdJson.value) {
        if (cd.$.name === name) {
          return (cd)
        }
      }
    } catch (e) {
      console.error('errore getCd', e)
      return ({})
    }
    return ({})
  }

  const updateSong = (s, i) => { // chiamarla con un nome che rimanda alle note, magari....
    const m = $store.getCurrentMix()
    // if (!i) i = this.get_index(s) non ho capito in che caso arrivo qui senza indice, per ora non ho copiato la get_index

    m.song[i].$.note = s.$.note
    $store.setCurrentMix(m)
  }

  const getSongByPath = (s, cdList) => {
    const path = s.$.path.split('/')[0]
    const songN = s.$.path.split('/')[1]
    const cd = getCd(path, cdList)

    // console.log('getSongByPath', path, cd)
    if (!cd.song) return undefined

    return (cd.song[parseInt(songN) - 1])
  }

  const getAllSongs = (cdList) => {
    const result = []
    for (const cd of cdList || $store.gCdJson.value) {
      // console.log(this.$store.g_cd_json[key].song)
      for (const song of cd.song) {
        const localSong = _cloneDeep(song)
        localSong.$.path = cd.$.name + '/' + song.$.track
        result.push(localSong)
      }
    }
    return result
  }

  return {
    loadCdFile,
    getCd,
    getAllSongs,
    updateSong,
    getSongByPath/* ,
    getCds,
    rimpiazzaMix */
  }
}
