export default function () {
  const getPreference = async (fileService, key) => {
    try {
      const pref = await fileService.read('preferences', 'preferences.json')
      const jPref = JSON.parse(pref)
      return jPref[key]
    } catch (e) {
      console.log(e)
      return ''
    }
  }

  const getPreferences = async (fileService) => {
    try {
      const pref = await fileService.read('preferences', 'preferences.json')
      const jPref = JSON.parse(pref)
      return jPref
    } catch (e) {
      console.log(e)
      return ''
    }
  }

  const setPreferences = async (fileService, value) => {
    try {
      await fileService.write('preferences', 'preferences.json', JSON.stringify(value), '', '')
    } catch (e) {
      console.log(e)
      return ''
    }
  }

  return {
    getPreference,
    getPreferences,
    setPreferences
  }
}
