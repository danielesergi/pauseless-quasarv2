import { api } from 'boot/axios'
import { fileService } from 'boot/fileService'
import usePreferences from './preferences.functions.js'
import { inject } from 'vue'

export default function () {
  const $store = inject('store')

  const getServerIp = async () => {
    const preferences = usePreferences()
    const serverIp = await preferences.getPreference(fileService, 'server_ip')
    return serverIp || '127.0.0.1'
  }
  const sendMix = async (mix, emitter) => {
    const serverIp = await getServerIp()
    try {
      const response = await api.post('http://' + serverIp + ':3000/json/mix', { mix: mix.$.name, xml: JSON.stringify(mix) })
      emitter.emit('notify-positive', 'Mix inviato con successo')
    } catch (e) {
      emitter.emit('notify-error', e)
    }
  }

  const scaricaXml = async (emitter) => {
    const prefs = { server_ip: await getServerIp() }

    let cdJson, mixJson
    try {
      const response = await api.get('http://' + prefs.server_ip + ':3000/json/cd')
      cdJson = response.data
      // console.log('cdJson dal server', cdJson)
      $store.commit('application/addLog', 'Caricamento cd andato a buon fine')
      emitter.emit('notify-positive', 'Lista cd scaricata')
    } catch (e) {
      $store.commit('application/addLog', e)
      console.error('download xml cd fallito', e)
      emitter.emit('notify-error', e)
    }
    try {
      const response = await api.get('http://' + prefs.server_ip + ':3000/json/mix')
      mixJson = response.data
      $store.commit('application/addLog', 'Caricamento mix andato a buon fine')
      emitter.emit('notify-positive', 'Lista mix scaricata')
    } catch (e) {
      $store.commit('application/addLog', e)
      console.error('download xml mix fallito', e)
      emitter.emit('notify-error', e)
    }
    if (fileService) {
      fileService.write('json_data', 'base_cd_list.json', JSON.stringify(cdJson), '', '')
        .then(
          () => {
            console.log('salvato cd file')
            emitter.emit('notify-positive', 'Lista cd salvata su file')
          },
          (e) => {
            console.error('errore salvataggio cd file', e)
            emitter.emit('notify-error', e)
          }
        )
      fileService.write('json_data', 'base_mix_list.json', JSON.stringify(mixJson), '', '')
        .then(
          () => {
            console.log('salvato mix file')
            emitter.emit('notify-positive', 'Lista mix salvata su file')
          },
          (e) => {
            console.error('errore salvataggio mix file', e)
            emitter.emit('notify-error', e)
          }
        )
    } else {
      console.error('Non sei su mobile!!')
    }
  }

  return {
    scaricaXml,
    sendMix
  }
}
