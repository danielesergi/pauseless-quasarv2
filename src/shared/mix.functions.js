import { api } from 'boot/axios'
import { Platform } from 'quasar'
import _cloneDeep from 'lodash/cloneDeep'
import _findIndex from 'lodash/findIndex'
import useCdFunctions from './cd.functions'
import { inject } from 'vue'

const getKeys = () => {
  const keys = {}
  keys.minor = []
  keys.major = []
  keys.minor.push({ key: 'G#-', position: 1, scalePosition: 9, mark: 'a' })
  keys.minor.push({ key: 'D#-', position: 2, scalePosition: 4, mark: 'a' })
  keys.minor.push({ key: 'A#-', position: 3, scalePosition: 11, mark: 'a' })
  keys.minor.push({ key: 'F-', position: 4, scalePosition: 6, mark: 'a' })
  keys.minor.push({ key: 'C-', position: 5, scalePosition: 1, mark: 'a' })
  keys.minor.push({ key: 'G-', position: 6, scalePosition: 8, mark: 'a' })
  keys.minor.push({ key: 'D-', position: 7, scalePosition: 3, mark: 'a' })
  keys.minor.push({ key: 'A-', position: 8, scalePosition: 10, mark: 'a' })
  keys.minor.push({ key: 'E-', position: 9, scalePosition: 5, mark: 'a' })
  keys.minor.push({ key: 'B-', position: 10, scalePosition: 12, mark: 'a' })
  keys.minor.push({ key: 'F#-', position: 11, scalePosition: 7, mark: 'a' })
  keys.minor.push({ key: 'C#-', position: 12, scalePosition: 2, mark: 'a' })

  keys.major.push({ key: 'B+', position: 1, scalePosition: 12, mark: 'b' })
  keys.major.push({ key: 'F#+', position: 2, scalePosition: 7, mark: 'b' })
  keys.major.push({ key: 'C#+', position: 3, scalePosition: 2, mark: 'b' })
  keys.major.push({ key: 'G#+', position: 4, scalePosition: 9, mark: 'b' })
  keys.major.push({ key: 'D#+', position: 5, scalePosition: 4, mark: 'b' })
  keys.major.push({ key: 'A#+', position: 6, scalePosition: 11, mark: 'b' })
  keys.major.push({ key: 'F+', position: 7, scalePosition: 6, mark: 'b' })
  keys.major.push({ key: 'C+', position: 8, scalePosition: 1, mark: 'b' })
  keys.major.push({ key: 'G+', position: 9, scalePosition: 8, mark: 'b' })
  keys.major.push({ key: 'D+', position: 10, scalePosition: 3, mark: 'b' })
  keys.major.push({ key: 'A+', position: 11, scalePosition: 10, mark: 'b' })
  keys.major.push({ key: 'E+', position: 12, scalePosition: 5, mark: 'b' })

  return keys
}

export default function () {
  const $store = inject('store')
  const cdFunc = useCdFunctions()

  const loadMixFile = async function () {
    /* if (Platform.is.cordova && Platform.is.mobile) {
      this.$fileService.read('json_data', 'base_mix_list.json')
        .then(
          (a) => {
            // console.log(JSON.parse(a))
            // console.log('caricato mix file')
            this.$store.g_mix_json = JSON.parse(a)
          },
          (e) => {
            console.error('errore get file', e)
            this.$store.g_mix_json = []
          }
        )
    } else { */
    try {
      const response = await api.get('json/mix')
      return response.data
    } catch (e) {
      console.error('errore get file mix (web)', e)
    }
    // }
  }

  const mixToSongList = (mixSongs, cdList) => {
    // console.log('inna mix to song list', mixSongs)
    const songs = mixSongs, ret = []
    let mixIndex = 0, oddEven = true
    for (const song of songs) {
      // console.log('inna mix to song list', key)
      let localSong = { $: {} }
      if (!song.$.type || song.$.type === 'local') {
        localSong = _cloneDeep(cdFunc.getSongByPath(song, cdList))
        // _.extend(song['$'], _.pick(localSong['$'], 'bpm', 'artist', 'key', 'title'))
        // console.log(localSong, this.getSongByPath(song), song, key)
        localSong.$.pause = localSong.pause
        // delete localSong.pause
        if (song.$.path.indexOf('/') > -1) { // TODO : questo è un mastrusso, eliminarlo prima o poi...
          localSong.$.path = song.$.path
        } else localSong.$.path = song.$.path + localSong.$.track

        localSong.$.note = song.$.note
        localSong.$.type = 'local'
        if (oddEven) localSong.$.class = 'odd'
        else localSong.$.class = 'even'
        oddEven = !oddEven
        localSong.$.song = localSong.$.artist + '/' + localSong.$.title
        // console.log('canzone normale', localSong.$)
      } else if (song.$.type && song.$.type === 'ext') {
        localSong.$ = { ...song.$ }
        if (oddEven) localSong.$.class = 'odd'
        else localSong.$.class = 'even'
        oddEven = !oddEven
        // console.log('canzone esterna', localSong.$)
      } else if (song.$.type && song.$.type === 'nota') {
        localSong.$ = { type: 'nota' }
        localSong.$.note = song.$.note
        localSong.$.class = 'currentmix-nota'
        // console.log('nota', localSong.$)
      }
      localSong.$.mixIndex = mixIndex
      mixIndex++

      ret.push(localSong)
    }
    return ret
  }

  const keys = {
    get: function () {
      return getKeys()
    },
    getFlat: function () {
      const ret = []
      const k = getKeys()
      for (const i of Object.keys(k)) {
        for (const y of k[i]) {
          ret.push(y.key)
        }
      }
      return ret
    },
    // 25/08/2023 : NUOVA VERSIONE. Prima erroneamente traslava le tonalità sulla camelot wheel, e non sulla normale scala delle note.
    // in pratica, prima aggiungendo 1 ad un F si otteneva C, ora si ottiene un F#
    keyAdd: function (k, scarto) { // sottrae anche...
      // console.log(k, k.indexOf('-'), scarto, this.get(), getKeys())
      // console.log('keyAdd', k, scarto, this.get())
      const ks = k.indexOf('-') > -1 ? this.get().minor : this.get().major // sceglie i maggiori o i minori a seconda di un '-' nel testo della chiave cercata
      const found = ks.find(el => el.key === k)
      // controllare if found??
      if (!found) {
        // console.error('chiave non trovata', ks, k, scarto)
        return 'k+'
      }
      let idx = found.scalePosition + scarto
      // let idx = _findIndex(ks, function (el) { return el.key === k }) + scarto
      // console.log(k, scarto, keys, idx)
      if (idx > 12) idx = idx - 12
      if (idx < 1) idx = idx + 12
      // console.log(JSON.stringify(keys), idx, k, scarto)
      // console.log(idx, ks.find(el => el.scalePosition === idx))
      // return ks[idx].key
      return ks.find(el => el.scalePosition === idx).key
    },
    keyAddCamelot: function (k, scarto) {
      // console.log(k, k.indexOf('-'), scarto, this.get(), getKeys())
      // console.log('keyAdd', k, scarto, this.get())
      const ks = k.indexOf('-') > -1 ? this.get().minor : this.get().major
      let idx = _findIndex(ks, function (el) { return el.key === k }) + scarto
      // console.log(k, scarto, keys, idx)
      if (idx > 11) idx = idx - 12
      if (idx < 0) idx = idx + 12
      // console.log(JSON.stringify(keys), idx, k, scarto)
      // console.log(idx, ks[idx])
      return ks[idx].key
    },
    getKeyM: function (k) {
      const keys = k.indexOf('-') > -1 ? getKeys().minor : getKeys().major,
        // mark = k.indexOf('-') > -1 ? 'a' : 'b',
        idx = _findIndex(keys, function (el) { return el.key === k })
      return (k.indexOf('-') > -1 ? getKeys().major : getKeys().minor)[idx].key
    },
    getPosition: function (k) {
      const ks = k.indexOf('-') > -1 ? getKeys().minor : getKeys().major
      return _findIndex(ks, function (el) { return el.key === k })
    }
  }

  const getByKey = (key, bpmS, bpmM, cdList) => {
    return new Promise(function (resolve) {
      // console.log('getByKey', key, bpmS, bpmM)
      // key = keys.keyAdd(key, parseInt(bpmM) - parseInt(bpmS))
      // console.log('transposed master key', key)

      const transposeKey = function (s) {
        // console.log('transposeKey', s)
        s.$.bpm_ref = bpmS
        if (bpmM === parseInt(s.$.bpm)) {
          s.$.scarto = 0
          return s.$.key
        } else {
          const scarto = Math.log2(bpmM / s.$.bpm) * 12 // bpmM - parseInt(s.$.bpm)
          s.$.scarto = scarto
          // console.log('calcolo scarto :', s.$.bpm, scarto, parseInt(scarto), Math.round(scarto), scartoMax)

          if (scarto < scartoMax) return keys.keyAdd(s.$.key, Math.round(scarto))
          else return 'k+' // una tonalita inesistente per far fallire il test di uguaglianza
        }
      }

      // v.$fileService.read('preferences', 'scarto_bpm').then(
      //  function (val) {
      let val = '3'
      if (val === 'testvalue') val = '3' // default per livereload
      const scartoMax = parseInt(val)

      // console.log('scarto', scartoMax)
      const retSongs = { key: [], key_before: [], key_after: [], key_m: [], keys: [], messages: '' }

      // primo giro, tonalità esatta
      try {
        for (const cd of cdList || $store.gCdJson.value) {
          console.log('cd in find in key tonalita esatta', cd)
          for (const songOriginal of cd.song) {
            const song = _cloneDeep(songOriginal)
            const t = transposeKey(song)
            song.$.transposed = t
            /* const t = song.$.key */
            if (key === t) {
              song.$.path = cd.$.name + '/' + song.$.track

              if (key === song.$.key && bpmS === song.$.bpm) song.natural_key_match = 1

              retSongs.key.push(song)
            }
            // console.log(song.$.key, song.$.bpm, transposeKey(song))
          }
        }
      } catch (e) {
        console.log('err', e)
        retSongs.messages += e + '\n'
      }
      retSongs.keys.push(key)

      // secondo giro, tonalità precedente
      try {
        for (const cd of cdList || $store.gCdJson.value) {
          for (const songOriginal of cd.song) {
            const song = _cloneDeep(songOriginal)
            const t = transposeKey(song)
            song.$.transposed = t
            if (keys.keyAddCamelot(key, -1) === t) {
              song.$.path = cd.$.name + '/' + song.$.track
              retSongs.key_before.push(song)
            }
            // console.log(song.$.key, song.$.bpm, transposeKey(song))
          }
        }
      } catch (e) {
        retSongs.messages += e + '\n'
      }
      retSongs.keys.push(keys.keyAddCamelot(key, -1))

      // terzo giro, tonalità successiva
      try {
        for (const cd of cdList || $store.gCdJson.value) {
          for (const songOriginal of cd.song) {
            const song = _cloneDeep(songOriginal)
            const t = transposeKey(song)
            song.$.transposed = t
            if (keys.keyAddCamelot(key, 1) === t) {
              song.$.path = cd.$.name + '/' + song.$.track
              retSongs.key_after.push(song)
            }
            // console.log(song.$.key, song.$.bpm, transposeKey(song))
          }
        }
      } catch (e) {
        retSongs.messages += e + '\n'
      }
      retSongs.keys.push(keys.keyAddCamelot(key, 1))

      try {
        // quarto giro, tonalità minore/maggiore corrispondente
        for (const cd of cdList || $store.gCdJson.value) {
          for (const songOriginal of cd.song) {
            const song = _cloneDeep(songOriginal)
            const t = transposeKey(song)
            song.$.transposed = t
            if (keys.getKeyM(key) === t) {
              song.$.path = cd.$.name + '/' + song.$.track
              retSongs.key_m.push(song)
            }
            // console.log(song.$.key, song.$.bpm, transposeKey(song))
          }
        }
      } catch (e) {
        retSongs.messages += e + '\n'
      }
      retSongs.keys.push(keys.getKeyM(key))
      // console.log('D+', keys.keyAdd('D+', 2), keys.keyAdd('D+', -2), keys.getKeyM('D+'))
      // console.log(JSON.stringify(retSongs))
      // return retSongs
      resolve(retSongs)
      //  },
      //  function (error) {
      //    console.log('Errore nel recupero dello scarto bpm ' + JSON.stringify(error))
      //    scartoMax = 3
      //  }
      // ).finally(function () {
      // })
    })
  }

  const getMix = (name, mixList) => {
    // console.log('get mix', $store.gMixJson)
    try {
      for (const mix of mixList || $store.gMixJson) {
        if (mix.$.name === name) {
          return (mix)
        }
      }
    } catch (e) {
      return ({})
    }
    return ({})
  }

  const loadMixIntoCurrentMix = (name) => {
    const m = getMix(name)
    console.log('mix caricato', m)
    $store.setCurrentMix(m)
  }

  const getPreviousNotes = (path, mixList) => { // path basta? solo nelle canzoni a catalogo comunque...
    const mixes = mixList || $store.gMixJson.value
    const retNotes = []
    for (const mix of mixes) {
      for (const song of mix.song) {
        if (song.$.path === path) {
          if (song.$.note) {
            retNotes.push(song.$.note)
          }
        }
      }
    }
    return retNotes
  }
  // console.log('aa', Math.log2(145 / 138) * 12)
  return {
    loadMixFile,
    mixToSongList,
    keys,
    getByKey,
    getMix,
    loadMixIntoCurrentMix,
    getPreviousNotes
  }
}
