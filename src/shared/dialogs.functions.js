import { Dialog } from 'quasar'
import ListDialog from 'components/ListDialog.vue'
import TextDialog from 'components/TextDialog.vue'
import PositionDialog from 'components/PositionDialog.vue'
import ExtSongDialog from 'components/ExtSongDialog.vue'
import BpmTitleDialog from 'components/BpmTitleDialog.vue'
import PreferencesDialog from 'components/PreferencesDialog.vue'

export default function () {
  const simpleListDialog = (title, list, action, emitter) => {
    Dialog.create({
      // cancel: true, se c'è il bottone annulla...io in teoria non ci voglio manco ok
      component: ListDialog,
      componentProps: {
        list, // lista da rappresentare, un array di stringhe
        action, // nome dell'evento a cui sottoscriversi per il click
        title, // titolo della dialog
        emitter // bus per gli eventi
      }
    }).onOk(() => {
      // console.log('>>>> OK')
    }).onCancel(() => {
      // console.log('>>>> Cancel')
    }).onDismiss(() => {
      // console.log('I am triggered on both OK and Cancel')
    })
  }

  const simpleTextDialog = (title, text, action, emitter) => {
    Dialog.create({
      // cancel: true, se c'è il bottone annulla...io in teoria non ci voglio manco ok
      component: TextDialog,
      componentProps: {
        text, // lista da rappresentare, un array di stringhe
        action, // nome dell'evento a cui sottoscriversi per il click
        title, // titolo della dialog
        emitter // bus per gli eventi
      }
      /* parent: this,
      text: value, // valore iniziale del testo
      action, // nome dell'evento a cui sottoscriversi per il click
      title // titolo della dialog */
    }).onOk(() => {
      // console.log('>>>> OK')
    }).onOk(() => {
      // console.log('>>>> second OK catcher')
    }).onCancel(() => {
      // console.log('>>>> Cancel')
    }).onDismiss(() => {
      // console.log('I am triggered on both OK and Cancel')
    })
  }

  const positionDialog = (emitter) => {
    Dialog.create({
      // cancel: true, se c'è il bottone annulla...io in teoria non ci voglio manco ok
      component: PositionDialog,
      componentProps: {
        title: 'Posizione',
        emitter
      }
    }).onOk(() => {
      // console.log('>>>> OK')
    }).onOk(() => {
      // console.log('>>>> second OK catcher')
    }).onCancel(() => {
      // console.log('>>>> Cancel')
    }).onDismiss(() => {
      // console.log('I am triggered on both OK and Cancel')
    })
  }

  const extSongDialog = (song, emitter) => {
    Dialog.create({
      // cancel: true, se c'è il bottone annulla...io in teoria non ci voglio manco ok
      component: ExtSongDialog,
      componentProps: {
        song,
        emitter,
        external: true
      }
    }).onOk(() => {
      // console.log('>>>> OK')
    }).onOk(() => {
      // console.log('>>>> second OK catcher')
    }).onCancel(() => {
      // console.log('>>>> Cancel')
    }).onDismiss(() => {
      // console.log('I am triggered on both OK and Cancel')
    })
  }

  const cdSongDialog = (song, emitter) => {
    Dialog.create({
      // cancel: true, se c'è il bottone annulla...io in teoria non ci voglio manco ok
      component: ExtSongDialog,
      componentProps: {
        song,
        emitter,
        external: false
      }
    }).onOk(() => {
      // console.log('>>>> OK')
    }).onOk(() => {
      // console.log('>>>> second OK catcher')
    }).onCancel(() => {
      // console.log('>>>> Cancel')
    }).onDismiss(() => {
      // console.log('I am triggered on both OK and Cancel')
    })
  }

  const bpmTitleDialog = (bpm, titolo, emitter) => {
    Dialog.create({
      // cancel: true, se c'è il bottone annulla...io in teoria non ci voglio manco ok
      component: BpmTitleDialog,
      componentProps: {
        bpm: parseInt(bpm),
        titolo,
        emitter
      }
    }).onOk(() => {
      // console.log('>>>> OK')
    }).onOk(() => {
      // console.log('>>>> second OK catcher')
    }).onCancel(() => {
      // console.log('>>>> Cancel')
    }).onDismiss(() => {
      // console.log('I am triggered on both OK and Cancel')
    })
  }

  const preferencesDialog = (serverIp, username, scartoBpm, emitter) => {
    console.log('dialog', serverIp, username, scartoBpm)
    Dialog.create({
      // cancel: true, se c'è il bottone annulla...io in teoria non ci voglio manco ok
      component: PreferencesDialog,
      componentProps: {
        serverIp, username, scartoBpm, emitter
      }
    }).onOk(() => {
      // console.log('>>>> OK')
    }).onOk(() => {
      // console.log('>>>> second OK catcher')
    }).onCancel(() => {
      // console.log('>>>> Cancel')
    }).onDismiss(() => {
      // console.log('I am triggered on both OK and Cancel')
    })
  }

  return {
    simpleListDialog,
    simpleTextDialog,
    positionDialog,
    extSongDialog,
    bpmTitleDialog,
    preferencesDialog,
    cdSongDialog
  }
}
