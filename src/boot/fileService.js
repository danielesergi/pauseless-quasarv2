// import something here
/* eslint-disable no-undef */

// "async" is optional
/* export default async ({ Vue }) => {
  // something to do
  document.addEventListener('deviceready', onDeviceReady, false)
  function onDeviceReady () {
    const service = {
      write: function (folder, name, data, beforeContent, afterContent) {
        return new Promise(function (resolve, reject) {
          // console.log('setto', folder, name)
          NativeStorage.setItem(folder + '/' + name, beforeContent + data + afterContent, resolve, reject)
        })
      },
      read: function (folder, name) {
        return new Promise(function (resolve, reject) {
          // console.log('leggo', folder, name)
          NativeStorage.getItem(folder + '/' + name, function (data) {
            // console.log('data retrieved', data)
            resolve(data)
          }, reject)
        })
      },
      list: function () { // fare in modo che legga le "sottodirectory" anche se virtuali
        return new Promise(function (resolve, reject) {
          NativeStorage.keys(resolve, reject)
        })
      },
      remove: function (key) {
        return new Promise(function (resolve, reject) {
          NativeStorage.remove(key, resolve, reject)
        })
      }
    }
    Vue.prototype.$fileService = service
  }
} */
import { boot } from 'quasar/wrappers'

let fileService, cdTest, mixTest
async function onDeviceReady () {
  fileService = {
    write: function (folder, name, data, beforeContent, afterContent) {
      return new Promise(function (resolve, reject) {
        console.log('setto', folder, name)
        NativeStorage.setItem(folder + '/' + name, beforeContent + data + afterContent, resolve, reject)
        /* NativeStorage.getItem(folder + '/' + name, function (data) {
          console.log('data retrieved post save', data)
          resolve(data)
        }, reject) */
      })
    },
    read: function (folder, name) {
      return new Promise(function (resolve, reject) {
        console.log('leggo', folder, name)
        NativeStorage.getItem(folder + '/' + name, function (data) {
          console.log('data retrieved', data)
          resolve(data)
        }, reject)
      })
    },
    list: function () { // fare in modo che legga le "sottodirectory" anche se virtuali
      return new Promise(function (resolve, reject) {
        NativeStorage.keys(resolve, reject)
      })
    },
    remove: function (folder, name) {
      return new Promise(function (resolve, reject) {
        NativeStorage.remove(folder + '/' + name, resolve, reject)
      })
    }
  }
  console.log('inizio lettura filesystem')
  try {
    cdTest = await fileService.read('json_data', 'base_cd_list.json')
    console.log('caricato cdTest', cdTest)
  } catch (e) {
    cdTest = '[]' // {"$":{"name":"da 1"},"song":[{"$":{"track":"1","bpm":"144","artist":"mad maxx vs peacemaker","wav":"wav","key":"G-","title":"god of neptunes - pauseless","used":1,"used_mixes":["Natale"]},"pause":["5.14-5.00","3.20-2.00"]}
    console.log('settato cdTest di default', e)
  }
  try {
    mixTest = await fileService.read('json_data', 'base_mix_list.json')
    console.log('caricato mixTest')
  } catch (e) {
    mixTest = '[]'
    console.log('settato mixTest di default', e)
  }
  // console.log('boot', cdTest)
}

export default boot(async ({ app }) => {
  // for use inside Vue files (Options API) through this.$axios and this.$api
  document.addEventListener('deviceready', () => {
    console.log('registro ondeviceready')
    onDeviceReady().then(() => {
      console.log('eseguo ondeviceready')
      app.config.globalProperties.$cd = JSON.parse(cdTest)
      app.config.globalProperties.$mix = JSON.parse(mixTest)
      app.config.globalProperties.$fileService = fileService
    })
  }, false)
})

export { fileService }
