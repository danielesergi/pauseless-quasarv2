export function setCdJson (state, f) {
  state.cdJson = f
}

export function updateSong (state, song) {
  for (const cd of state.cdJson) {
    if (cd.$.name === song.$.cd) {
      // console.log(cd.song[parseInt(song.$.track) - 1])
      cd.song[parseInt(song.$.track) - 1] = song
    }
  }
}

export function addCd (state) {
  state.cdJson.unshift({
    $: {
      name: 'new cd'
    },
    song: []
  })
  console.log(state.cdJson[0])
}
