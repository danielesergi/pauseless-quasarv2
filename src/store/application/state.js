export default function () {
  return {
    loading: false,
    selectedSong: {},
    selectedSongIndex: -1,
    loadLog: '',
    // eslint-disable-next-line no-unneeded-ternary
    unsaved: localStorage.getItem('unsaved') === 'true' ? true : false
  }
}
