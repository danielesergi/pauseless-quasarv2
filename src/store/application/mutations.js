Storage.prototype.setObject = function (key, value) {
  // console.log('circular', inspect(value))
  this.setItem(key, JSON.stringify(value))
}

export function setSelectedSong (state, song) {
  state.selectedSong = song
}

export function setSelectedSongIndex (state, i) {
  state.selectedSongIndex = i
}

export function setUnsaved (state, v) {
  state.unsaved = v
  localStorage.setItem('unsaved', v)
}

export function addLog (state, message) {
  state.loadLog += message + '\n'
}

export function toggleLoading (state) {
  state.loading = !state.loading
}
