import { store } from 'quasar/wrappers'
import { createStore } from 'vuex'
import currentMix from './current-mix'
import cdJson from './cd-json'
import mixJson from './mix-json'
import application from './application'
// import example from './module-example'

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default store(function (/* { ssrContext } */) {
  console.log('istanzio lo store vuex')
  const Store = createStore({
    modules: {
      currentMix, cdJson, mixJson, application
    },

    // enable strict mode (adds overhead!)
    // for dev mode and --debug builds only
    strict: process.env.DEBUGGING
  })

  return Store
})
