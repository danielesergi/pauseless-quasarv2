Storage.prototype.getObject = function (key) {
  const value = this.getItem(key)
  try {
    // eslint-disable-next-line no-eval
    return value === 'undefined' ? undefined : JSON.parse(value)
  } catch (e) {
    console.log('errore parse', value)
    // eslint-disable-next-line no-eval
    return eval(value)
  }
}
export default function () {
  return {
    currentMix: localStorage.getObject('current_mix') || { $: { name: 'current mix', description: '', bpm: '145' }, song: [] },
    selectedSong: {},
    selectedSongIndex: -1
  }
}
