Storage.prototype.setObject = function (key, value) {
  // console.log('circular', inspect(value))
  this.setItem(key, JSON.stringify(value))
}

export function setTitle (state, title) {
  state.currentMix.$.name = title
  localStorage.setObject('current_mix', state.currentMix)
}

export function setBpm (state, bpm) {
  state.currentMix.$.bpm = bpm
  localStorage.setObject('current_mix', state.currentMix)
}

export function updateCurrentMix (state, mix) {
  state.currentMix = mix
  localStorage.setObject('current_mix', state.currentMix)
}

export function removeFromCurrentMix (state) {
  state.currentMix.song.splice(parseInt(state.selectedSongIndex), 1)
  localStorage.setObject('current_mix', state.currentMix)
}

export function swapSongs (state, direction) {
  const pos1 = state.selectedSongIndex
  let pos2
  if (direction === 'down') pos2 = pos1 + 1
  else pos2 = pos1 - 1

  const temp = state.currentMix.song[pos2]
  state.currentMix.song[pos2] = state.currentMix.song[pos1]
  state.currentMix.song[pos1] = temp

  if (direction === 'down') state.selectedSongIndex++
  else state.selectedSongIndex--
  localStorage.setObject('current_mix', state.currentMix)
}

export function addToCurrentMix (state, song) {
  state.currentMix.song.push({ $: { type: 'local', path: song.$.cd + '/' + song.$.track, note: '' } })
  localStorage.setObject('current_mix', state.currentMix)
}

export function addToCurrentMixIdx (state, { song, idx }) {
  state.currentMix.song.splice(idx, 0, { $: { path: song.$.cd + '/' + song.$.track, note: '' } })
  localStorage.setObject('current_mix', state.currentMix)
}

export function replaceInCurrentMix (state, { song, idx }) {
  state.currentMix.song[idx] = { $: { path: song.$.cd + '/' + song.$.track, note: '' } }
  localStorage.setObject('current_mix', state.currentMix)
}

export function addToCurrentMixExt (state, n) {
  state.currentMix.song.push({ $: { type: 'ext', artist: n.artist, title: n.title, bpm: n.bpm, key: n.key, note: '' } })
  localStorage.setObject('current_mix', state.currentMix)
}

export function updateCurrentMixExt (state, s) {
  s.type = 'ext'
  state.currentMix.song[state.selectedSongIndex].$ = s
  localStorage.setObject('current_mix', state.currentMix)
}

export function addToCurrentMixNote (state, note) {
  state.currentMix.song.push({ $: { type: 'nota', note: note } })
  localStorage.setObject('current_mix', state.currentMix)
}

export function updateCurrentMixNote (state, note) {
  state.currentMix.song[state.selectedSongIndex].$.note = note
  localStorage.setObject('current_mix', state.currentMix)
}

export function clearCurrentMix (state) {
  state.currentMix = {
    song: [],
    $: {
      name: 'current mix',
      description: '',
      bpm: '145'
    }
  }
  localStorage.setObject('current_mix', state.currentMix)
}

export function setSelectedSong (state, song) {
  state.selectedSong = song
}

export function setSelectedSongIndex (state, i) {
  state.selectedSongIndex = i
}
