import { inject, computed, onBeforeUnmount } from 'vue'
import loadMixFile from '../../shared/mix.functions.js'
import { useStore } from 'vuex'
import dialogs from '../../shared/dialogs.functions.js'
import { useRouter } from 'vue-router'

export default function (props) {
  const $store = useStore()
  const router = useRouter()
  const $actionsheets = inject('actionsheets')
  const emitter = inject('emitter')
  const mixFunc = loadMixFile()
  const dialogsFunc = dialogs()

  emitter.once('add-to-mix', e => {
    // console.log('ricevuto evento add-to-mix', e)
    $store.commit('currentMix/addToCurrentMix', e)
  })

  const mix = computed(() => {
    // console.log(mixFunc.getMix(props.name))
    return mixFunc.getMix(props.name, $store.state.mixJson.mixJson)
  })

  const songs = computed(() => {
    return mixFunc.mixToSongList(mix.value.song, $store.state.cdJson.cdJson)
  })

  const selectSong = function (s, i) {
    const selected = { ...s }
    $store.commit('currentMix/setSelectedSong', selected)
    $store.commit('currentMix/setSelectedSongIndex', i)
    // console.log('select song', selectedSong.value)
    $actionsheets.showSheet('selectSong', emitter, selected)
  }

  const viewMixes = function (m) {
    console.log('view mixes', m.used_mixes)
    dialogsFunc.simpleListDialog('Mixes', m.used_mixes, 'go-to-mix', emitter)
  }

  emitter.once('go-to-mix', e => {
    // console.log('ricevuto evento go-to-mix', e)
    router.push({ path: '/mix/' + e })
  })

  onBeforeUnmount(() => {
    emitter.off('go-to-mix')
    emitter.off('add-to-mix')
  })

  return {
    mix,
    songs,
    selectSong,
    viewMixes,
    emitter
  }
}
