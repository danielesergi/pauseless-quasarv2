import { reactive, inject, computed, onBeforeUnmount } from 'vue'
import loadCdFile from '../../shared/cd.functions.js'
import dialogs from '../../shared/dialogs.functions.js'
// import _cloneDeep from 'lodash/cloneDeep'
import { useRouter } from 'vue-router'
import { useStore } from 'vuex'

export default function (props) {
  // const $store = inject('store')
  const $store = useStore()
  const $actionsheets = inject('actionsheets')
  const emitter = inject('emitter')
  const loading = false // ref($store.loading.value)
  const cdFunc = loadCdFile()
  const dialogsFunc = dialogs()
  const selectedSong = reactive({})
  const router = useRouter()

  emitter.once('add-to-mix', e => {
    // console.log('ricevuto evento add-to-mix', e)
    $store.commit('currentMix/addToCurrentMix', e)
    // cdFunc.aggiungiMix(e)
    router.push({ path: '/current-mix' })
  })

  /* emitter.once('go-to-mix', e => {
    // console.log('ricevuto evento go-to-mix', e)
    router.push({ path: '/mix/' + e })
  }) */

  emitter.once('dialog-cd-song', e => {
    // console.log('ricevuto evento dialog-cd-song', e)
    dialogsFunc.cdSongDialog(e, emitter)
  })

  emitter.once('update-song', e => {
    console.log('ricevuto evento update-song', e)
    // $store.commit('currentMix/updateCurrentMixExt', e)
  })

  const cd = computed(() => {
    // console.log(cdFunc.getCd(props.name))
    return cdFunc.getCd(props.name, $store.state.cdJson.cdJson)
  })

  const selectSong = function (s, i) {
    const selected = { ...s }
    $store.commit('currentMix/setSelectedSong', selected)
    $store.commit('currentMix/setSelectedSongIndex', i)
    // console.log('select song', selectedSong.value)
    $actionsheets.showSheet('selectSong', emitter, selected)
  }

  // const viewMixes = function (m) {
  /* emitter.once('view-mixes', e => {
    console.log('view mixes', e.used_mixes)
    dialogsFunc.simpleListDialog('Mixes', e.used_mixes, 'go-to-mix', emitter)
  }) */

  onBeforeUnmount(() => {
    emitter.off('add-to-mix')
    /* emitter.off('go-to-mix') */
    emitter.off('dialog-cd-song')
    emitter.off('update-song')
    /* emitter.off('view-mixes') */
  })

  return {
    loading,
    cd,
    selectSong,
    emitter,
    selectedSong
  }
}
