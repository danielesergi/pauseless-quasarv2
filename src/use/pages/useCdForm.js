import { reactive, inject, computed, ref, onBeforeUnmount } from 'vue'
import loadCdFile from '../../shared/cd.functions.js'
import loadMixFile from '../../shared/mix.functions.js'
import dialogs from '../../shared/dialogs.functions.js'
// import _cloneDeep from 'lodash/cloneDeep'
import { useRouter } from 'vue-router'
import { useStore } from 'vuex'

export default function (props) {
  // const $store = inject('store')
  const $store = useStore()
  const $actionsheets = inject('actionsheets')
  const emitter = inject('emitter')
  const loading = false // ref($store.loading.value)
  const cdFunc = loadCdFile()
  const mixFunc = loadMixFile()
  const dialogsFunc = dialogs()
  const selectedSong = reactive({})
  const router = useRouter()
  // const keys = require('./routes/keys')
  const keys = mixFunc.keys

  emitter.once('add-to-mix', e => {
    // console.log('ricevuto evento add-to-mix', e)
    $store.commit('currentMix/addToCurrentMix', e)
    // cdFunc.aggiungiMix(e)
    router.push({ path: '/current-mix' })
  })

  emitter.once('go-to-mix', e => {
    console.log('ricevuto evento go-to-mix', e)
    // v.$router.push({ path: '/current-mix' })
  })

  emitter.once('dialog-cd-song', e => {
    // console.log('ricevuto evento dialog-cd-song', e)
    dialogsFunc.cdSongDialog(e, emitter)
  })

  emitter.once('song-form-change', e => {
    // console.log('ricevuto evento song-form-change', e)
    $store.commit('cdJson/updateSong', { ...e })
  })

  const cd = computed(() => {
    // console.log(cdFunc.getCd(props.name), $store.state.cdJson.cdJson)
    return cdFunc.getCd(props.name, $store.state.cdJson.cdJson)
  })

  const selectSong = function (s, i) {
    const selected = { ...s }
    $store.commit('currentMix/setSelectedSong', selected)
    $store.commit('currentMix/setSelectedSongIndex', i)
    // console.log('select song', selectedSong.value)
    $actionsheets.showSheet('selectSong', emitter, selected)
  }

  const viewMixes = function (m) {
    console.log('view mixes', m.used_mixes)
    dialogsFunc.simpleListDialog('Mixes', m.used_mixes, 'go-to-mix', emitter)
  }

  const wavOptions = [
    'wav', 'mp3'
  ]

  const keyOptions = keys.get()

  onBeforeUnmount(() => {
    emitter.off('add-to-mix')
    emitter.off('go-to-mix')
    emitter.off('dialog-cd-song')
    emitter.off('song-form-change')
  })

  return {
    loading,
    cd,
    cdCopy: Object.assign({}, cd.value),
    selectSong,
    viewMixes,
    emitter,
    selectedSong,
    wavOptions,
    keyOptions,
    model: ref('')
  }
}
