
import { inject, computed, ref, reactive, getCurrentInstance, onBeforeUnmount } from 'vue'
import useMixFunctions from '../../shared/mix.functions.js'
import useServerFunctions from '../../shared/server.functions.js'
import useDialogs from '../../shared/dialogs.functions.js'
import { useRouter } from 'vue-router'
import { useStore } from 'vuex'

export default function () {
  // const $store = inject('store')
  const emitter = inject('emitter')
  const mixFunc = useMixFunctions()
  // const cdFunc = useCdFunctions()
  const dialogs = useDialogs()
  const router = useRouter()
  const selectedSong = reactive({}) // reactive($store.selectedSong.value)
  const $actionsheets = inject('actionsheets')
  const selectedSongIndex = ref(-1) // ref($store.selectedSongIndex)
  const serverFunc = useServerFunctions()
  const internalInstance = getCurrentInstance()

  /* area di test per il nuovo store */
  const $store = useStore()
  const songsVuex = computed({
    get: () => mixFunc.mixToSongList($store.state.currentMix.currentMix.song, $store.state.cdJson.cdJson),
    set: val => {
      $store.commit('currentMix/updateCurrentMix', val)
    }
  })

  /* area di test */

  const selectSong = function (s, i) {
    $store.commit('currentMix/setSelectedSong', { ...s })
    $store.commit('currentMix/setSelectedSongIndex', i)
    if (s.$.type === 'local') {
      $actionsheets.showSheet('song', emitter, { ...s })
    } else if (s.$.type === 'ext') {
      $actionsheets.showSheet('song-ext', emitter, { ...s })
    } else if (s.$.type === 'nota') {
      $actionsheets.showSheet('nota', emitter, { ...s })
    }
  }

  const bpm = computed(() => {
    return $store.state.currentMix.currentMix.$ ? $store.state.currentMix.currentMix.$.bpm : 145
  })
  const title = computed(() => {
    return $store.state.currentMix.currentMix.$ ? $store.state.currentMix.currentMix.$.name : 'titoloprova'
  })

  emitter.once('edit-song-note', e => {
    // console.log('ricevuto evento edit-song-note', $store.selectedSong.value, e)
    dialogs.simpleTextDialog('Salva note', (e.$.note ? e.$.note : ''), 'saved-song-note', emitter)
  })

  emitter.once('saved-song-note', e => {
    // console.log('ricevuto evento saved-song-note', $store.selectedSong.value, $store.selectedSongIndex, e)
    $store.commit('currentMix/updateCurrentMixNote', e)
  })

  emitter.once('previous-notes', e => {
    // console.log('ricevuto evento previous-notes', $store.selectedSong.value, $store.selectedSongIndex, e)
    const notesArr = mixFunc.getPreviousNotes($store.state.currentMix.selectedSong.$.path, $store.state.mixJson.mixJson)
    dialogs.simpleListDialog('Note precedenti', notesArr, 'select-previous-note', emitter)
  })

  emitter.once('select-previous-note', e => {
    // console.log('ricevuto evento select-previous-note', e)
    $store.commit('currentMix/updateCurrentMixNote', e)
  })

  emitter.once('find-in-key', e => {
    // console.log('ricevuto evento find-in-key', $store.selectedSong.value)
    router.push({ path: '/find-in-key/' + $store.state.currentMix.selectedSong.$.bpm + '/' + $store.state.currentMix.selectedSong.$.key.replace('#', '*') + '/' + $store.state.currentMix.selectedSongIndex })
  })

  emitter.once('replace-in-key', e => {
    // console.log('ricevuto evento find-in-key', $store.selectedSong.value)
    router.push({ path: '/replace-in-key/' + $store.state.currentMix.selectedSong.$.bpm + '/' + $store.state.currentMix.selectedSong.$.key.replace('#', '*') + '/' + $store.state.currentMix.selectedSongIndex })
  })

  emitter.once('show-position-dialog', e => {
    console.log('ricevuto evento show-position-dialog', $store.state.currentMix.selectedSong, e)
    dialogs.positionDialog(emitter)
  })

  const restoreMixIndex = () => {
    for (const [idx, song] of songsVuex.value.entries()) {
      song.$.mixIndex = idx
    }
  }

  emitter.once('remove-from-mix', e => {
    $store.commit('currentMix/removeFromCurrentMix', $store.state.currentMix.selectedSongIndex)
    restoreMixIndex()
  })

  emitter.once('move-up', e => {
    console.log('move-up', $store.selectedSongIndex, $store.selectedSongIndex - 1)
    if ($store.state.currentMix.selectedSongIndex > 0) {
      $store.commit('currentMix/swapSongs', 'up')
    }
  })

  emitter.once('move-down', e => {
    console.log('move-down', $store.selectedSongIndex, $store.selectedSongIndex + 1)
    if ($store.state.currentMix.selectedSongIndex < $store.state.currentMix.currentMix.song.length - 1) {
      $store.commit('currentMix/swapSongs', 'down')
    }
  })

  emitter.once('dialog-ext-song', e => {
    // console.log('ricevuto evento dialog-ext-song', e)
    if (e && e.$ && e.$.type === 'ext') {
      dialogs.extSongDialog(e, emitter)
    } else {
      dialogs.extSongDialog({ no: 'song' }, emitter) // canzone farlocca, dovrei inventare un metodo più furbo...
    }
  })

  emitter.once('save-ext-song', e => {
    // console.log('ricevuto evento save-ext-song', e)
    $store.commit('currentMix/addToCurrentMixExt', e)
  })

  emitter.once('update-ext-song', e => {
    // console.log('ricevuto evento update-ext-song', e)
    $store.commit('currentMix/updateCurrentMixExt', e)
  })

  emitter.once('dialog-note-song', e => {
    // console.log('ricevuto evento dialog-note-song', e)
    if (e && e.$ && e.$.type === 'nota') {
      dialogs.simpleTextDialog('Modifica nota', e.$.note, 'update-note', emitter)
    } else {
      dialogs.simpleTextDialog('Aggiungi nota', '', 'insert-note', emitter)
    }
  })

  emitter.once('insert-note', e => {
    // console.log('ricevuto evento insert-note', e)
    $store.commit('currentMix/addToCurrentMixNote', e)
  })

  emitter.once('update-note', e => {
    // console.log('ricevuto evento update-note', e, $store.selectedSong.value)
    // $store.selectedSong.value.$.note = e
    // cdFunc.updateSong($store.selectedSong.value, $store.selectedSongIndex)
    $store.commit('currentMix/updateCurrentMixNote', e)
  })

  emitter.once('clear-mix', e => {
    // console.log('ricevuto evento clear-mix')
    $store.commit('currentMix/clearCurrentMix', e)
  })

  emitter.once('send-mix', e => {
    // console.log('ricevuto evento save-mix')
    const mix = $store.state.currentMix.currentMix
    serverFunc.sendMix(mix, emitter)
  })

  emitter.once('show-sendmixas-dialog', e => {
    // console.log('ricevuto evento save-mix-as')
    dialogs.simpleTextDialog('Nome mix', ($store.state.currentMix.currentMix.$.name ? $store.state.currentMix.currentMix.$.name : ''), 'send-mix-as', emitter)
  })

  emitter.once('send-mix-as', e => {
    $store.commit('currentMix/setTitle', e)
    const mix = $store.state.currentMix.currentMix
    serverFunc.sendMix(mix, emitter)
  })

  emitter.once('save-mix', e => {
    // console.log('ricevuto evento save-mix')
    const mix = $store.state.currentMix.currentMix
    internalInstance.appContext.config.globalProperties.$fileService.write('json_data', 'mix_' + mix.$.name + '.json', JSON.stringify(mix), '', '')
  })

  emitter.once('save-mix-to-temp', e => {
    // console.log('ricevuto evento save-mix-to-temp')
    dialogs.simpleTextDialog('Nome mix', ($store.state.currentMix.currentMix.$.name ? $store.state.currentMix.currentMix.$.name : ''), 'send-mix-as', emitter)
  })

  emitter.once('show-savemixas-dialog', e => {
    // console.log('ricevuto evento save-mix-as')
    dialogs.simpleTextDialog('Nome mix', ($store.state.currentMix.currentMix.$.name ? $store.state.currentMix.currentMix.$.name : ''), 'save-mix-as', emitter)
  })

  emitter.once('save-mix-as', e => {
    $store.commit('currentMix/setTitle', e)
    const mix = $store.state.currentMix.currentMix
    internalInstance.appContext.config.globalProperties.$fileService.write('json_data', 'mix_' + mix.$.name + '.json', JSON.stringify(mix), '', '')
  })

  onBeforeUnmount(() => {
    emitter.off('edit-song-note')
    emitter.off('saved-song-note')
    emitter.off('previous-notes')
    emitter.off('select-previous-note')
    emitter.off('find-in-key')
    emitter.off('replace-in-key')
    emitter.off('show-position-dialog')
    emitter.off('remove-from-mix')
    emitter.off('move-up')
    emitter.off('move-down')
    emitter.off('dialog-ext-song')
    emitter.off('save-ext-song')
    emitter.off('update-ext-song')
    emitter.off('dialog-note-song')
    emitter.off('insert-note')
    emitter.off('update-note')
    emitter.off('clear-mix')
    emitter.off('send-mix')
    emitter.off('show-sendmixas-dialog')
    emitter.off('send-mix-as')
    emitter.off('save-mix')
    emitter.off('show-savemixas-dialog')
    emitter.off('save-mix-as')
  })

  return {
    bpm,
    title,
    songs: songsVuex,
    selectSong,
    selectedSong,
    selectedSongIndex
  }
}
