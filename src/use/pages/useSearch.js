import { computed, inject, reactive, ref, onBeforeUnmount } from 'vue'
import useCdFunctions from '../../shared/cd.functions.js'
import _cloneDeep from 'lodash/cloneDeep'
import { useRouter } from 'vue-router'
import { useStore } from 'vuex'
import useDialogs from '../../shared/dialogs.functions.js'

export default function () {
  const cdFunc = useCdFunctions()
  const $store = useStore()
  const $actionsheets = inject('actionsheets')
  const emitter = inject('emitter')
  const router = useRouter()
  const selectedSong = reactive({})
  const develop = false
  const model = ref(null)
  const bpmmodel = ref(null)
  const keymodel = ref(null)
  const usate = ref(false)
  const dialogs = useDialogs()

  const cd = computed(() => {
    return cdFunc.getAllSongs($store.state.cdJson.cdJson)
  })

  emitter.once('add-to-mix', e => {
    // console.log('ricevuto evento add-to-mix', e)
    $store.commit('currentMix/addToCurrentMix', e)
    // cdFunc.aggiungiMix(e)
    router.push({ path: '/current-mix' })
  })

  emitter.once('go-to-mix', e => {
    // console.log('ricevuto evento go-to-mix', e)
    router.push({ path: '/mix/' + e })
  })

  /* const selectSong = function (s, i) {
    s.$.cd = s.$.path.split('/')[0]
    selectedSong.value = _cloneDeep(s)
    // console.log('select song', selectedSong.value)
    $actionsheets.showSheet('selectSong', emitter, selectedSong.value)
  } */
  const selectSong = function (s, i) {
    const selected = { ...s }
    $store.commit('currentMix/setSelectedSong', selected)
    $store.commit('currentMix/setSelectedSongIndex', i)
    // console.log('select song', selectedSong.value)
    $actionsheets.showSheet('selectSong', emitter, selected)
  }

  const filterFn = function (val, update, abort) {
    update(() => {
      const needle = val.toLocaleLowerCase()
      options = stringOptions.value.filter(v => v.toLocaleLowerCase().indexOf(needle) > -1)
    })
  }

  const setModel = function (val) {
    model.value = val
  }

  const stringOptions = computed(() => {
    const a = []
    for (const s of cd.value) {
      if (a.indexOf(s.$.artist) === -1) a.push(s.$.artist)
    }
    for (const s of cd.value) {
      if (a.indexOf(s.$.title) === -1) a.push(s.$.title)
    }
    for (const s of cd.value) {
      if (a.indexOf(s.$.bpm) === -1) a.push(s.$.bpm)
    }
    for (const s of cd.value) {
      if (a.indexOf(s.$.key) === -1) a.push(s.$.key)
    }
    return a
  })

  const checkTitleArtist = (terms, song) => {
    try {
      const checkValues = terms.toLowerCase().trim().split(' ')
      let matched = false
      for (const val of checkValues) {
        if (song.$.title.indexOf(val) > -1 ||
          song.$.artist.indexOf(val) > -1) matched = true
      }
      return matched
    } catch (e) {
      console.error(e)
      return false
    }
  }

  const filteredSongs = computed(() => {
    if ((!model.value || model.value.length < 3) &&
      (!bpmmodel.value || bpmmodel.value.length < 3) &&
      (!keymodel.value || keymodel.value.length < 2)) return cd.value.slice(0, 10)
    else {
      return cd.value.filter(song => {
        if (
          (((model.value && (checkTitleArtist(model.value, song))
          ) || !model.value)) &&
          (((bpmmodel.value && (
            song.$.bpm.indexOf(bpmmodel.value) > -1)
          ) || !bpmmodel.value)) &&
          (((keymodel.value && (
            song.$.key.toLowerCase().indexOf(keymodel.value.toLowerCase()) > -1)
          ) || !keymodel.value)) &&
          (((usate.value && (
            song.$.used_mixes && song.$.used_mixes.length > 0)
          ) || !usate.value))) return true

        return false
      })
      /* if (model.value) {
        const checkValues = model.value.toLowerCase().trim().split(' ')
        return cd.value.filter(song => { // così sono in or, mi piacerebbe fossero in and ma non so come fare
          for (const val of checkValues) {
            if (
              (
                song.$.title.indexOf(val) > -1 ||
                song.$.artist.indexOf(val) > -1 ||
                song.$.bpm.indexOf(val) > -1 ||
                song.$.key.indexOf(val) > -1
              ) && (
                ((bpmmodel.value && (
                  song.$.bpm.indexOf(bpmmodel.value) > -1 ||
                  song.$.key.indexOf(bpmmodel.value) > -1)
                ) || !bpmmodel.value)
              ) && (
                ((keymodel.value && (
                  song.$.bpm.indexOf(keymodel.value) > -1 ||
                  song.$.key.indexOf(keymodel.value) > -1)
                ) || !keymodel.value)
              )
            ) {
              if (usate.value && song.$.used_mixes && song.$.used_mixes.length > 0) {
                return true
              } else if (!usate.value) return true
            }
          }
          return false
          // return song.$.title.indexOf(model.value) > -1 || song.$.artist.indexOf(model.value) > -1 || song.$.bpm.indexOf(model.value) > -1 || song.$.key.indexOf(model.value) > -1
        })
      } else {
        return cd.value.filter(song => {
          if ((bpmmodel.value && (
            song.$.bpm.indexOf(bpmmodel.value) > -1 ||
            song.$.key.indexOf(bpmmodel.value) > -1)
          ) || !bpmmodel.value) {
            if (usate.value && song.$.used_mixes && song.$.used_mixes.length > 0) {
              return true
            } else if (!usate.value) return true
          } else {
            return false
          }

          return false
        })
      } */
    }
  })

  let options = stringOptions.value

  const viewMixes = (m) => {
    console.log('view mixes', m)
    dialogs.simpleListDialog('Mix', m, 'go-to-mix', emitter)
    // this.simpleListDialog('Mixes', m)
  }

  onBeforeUnmount(() => {
    emitter.off('add-to-mix')
    emitter.off('go-to-mix')
  })

  return {
    cd,
    selectSong,
    selectedSong,
    filterFn,
    setModel,
    stringOptions,
    filteredSongs,
    develop,
    model,
    bpmmodel,
    keymodel,
    options,
    usate,
    viewMixes
  }
}
