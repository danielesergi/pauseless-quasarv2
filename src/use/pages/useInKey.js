import useMixFunctions from '../../shared/mix.functions.js'
import { ref, reactive, onMounted, computed } from 'vue'
import { useRouter } from 'vue-router'
import { useStore } from 'vuex'

export default function (props) {
  const $store = useStore()
  const mixFunc = useMixFunctions()
  const router = useRouter()
  const songsKey = reactive([])
  const songsKeyAfter = reactive([])
  const songsKeyBefore = reactive([])
  const songsKeyRelative = reactive([])
  const keyAfter = ref('')
  const keyBefore = ref('')
  const keyRelative = ref('')
  const properties = reactive({ titolo: '' })
  const isReplace = router.currentRoute._value.path.indexOf('replace') > -1

  onMounted(async () => {
    console.log('onMounted InKey', props.selectedKey.replace('*', '#'), props.bpm, $store.state.currentMix.currentMix.$.bpm, properties)
    const songs = await mixFunc.getByKey(props.selectedKey.replace('*', '#'), props.bpm, $store.state.currentMix.currentMix.$.bpm, $store.state.cdJson.cdJson)
    console.log(songs)

    properties.titolo = props.selectedKey.replace('*', '#') + ' ' + props.bpm + '->' + mixFunc.keys.keyAdd(props.selectedKey, parseInt($store.state.currentMix.currentMix.$.bpm) - parseInt(props.bpm)) + ' ' + $store.state.currentMix.currentMix.$.bpm

    songsKey.value = songs.key
    songsKeyBefore.value = songs.key_before
    songsKeyAfter.value = songs.key_after
    songsKeyRelative.value = songs.key_m
    keyBefore.value = songs.keys[1]
    keyAfter.value = songs.keys[2]
    keyRelative.value = songs.keys[3]
  })

  const songsKeyComp = computed(() => {
    return songsKey.value
  })
  const songsKeyBeforeComp = computed(() => {
    return songsKeyBefore.value
  })
  const songsKeyAfterComp = computed(() => {
    return songsKeyAfter.value
  })
  const songsKeyRelativeComp = computed(() => {
    return songsKeyRelative.value
  })
  const keyBeforeComp = computed(() => {
    return keyBefore.value
  })
  const keyAfterComp = computed(() => {
    return keyAfter.value
  })
  const keyRelativeComp = computed(() => {
    return keyRelative.value
  })

  const selectSong = function (s, i) {
    // console.log('aggiungiMixIdx', s, s.$.path.split('/')[0], parseInt(this.idx) + 1)
    // s.cd = s.$.path.split('/')[0]
    const localSong = { ...s }
    if (isReplace) {
      const localIndex = parseInt(props.idx)
      $store.commit('currentMix/replaceInCurrentMix', { song: localSong, idx: localIndex })
    } else {
      const localIndex = parseInt(props.idx) + 1
      $store.commit('currentMix/addToCurrentMixIdx', { song: localSong, idx: localIndex })
    }
    //  cdFunc.aggiungiMixIdx(s, parseInt(props.idx) + 1)
    router.push({ path: '/current-mix' })
  }

  return {
    keyBefore: keyBeforeComp,
    keyAfter: keyAfterComp,
    keyRelative: keyRelativeComp,
    properties,
    songsKey: songsKeyComp,
    songsKeyBefore: songsKeyBeforeComp,
    songsKeyAfter: songsKeyAfterComp,
    songsKeyRelative: songsKeyRelativeComp,
    selectSong
  }
}
