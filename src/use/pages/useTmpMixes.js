import { api } from 'boot/axios'
import { ref, inject } from 'vue'

export default function () {
  const mixes = ref([])
  const cds = ref([])
  const emitter = inject('emitter')

  const dumpMix = async (mix) => {
    try {
      await api.get('/tmpmix/save/' + mix)
      emitter.emit('set-unsaved')
      emitter.emit('notify-positive', 'Mix salvato in memoria')
      removeMix(mix)
    } catch (e) {
      emitter.emit('notify-error', e)
      console.log(e)
    }
  }

  const removeMix = async (mix) => {
    try {
      await api.get('/tmpmix/remove/' + mix)
      emitter.emit('notify-positive', 'Mix eliminato')
      loadTmpMix()
    } catch (e) {
      emitter.emit('notify-error', e)
      console.log(e)
    }
  }

  const loadTmpMix = async () => {
    try {
      const tmp = await api.get('json/tmpmix')
      console.log('caricati mix temporanei da web (use)')
      mixes.value = tmp.data
      // for (const item of tmp.data) mixes.push(item)

      console.log(mixes)
    } catch (e) {
      console.log(e)
    }
  }
  loadTmpMix()

  const loadTmpCd = async () => {
    try {
      const tmp = await api.get('json/tmpcd')
      console.log('caricati cd temporanei da web (use)')
      cds.value = tmp.data
      // for (const item of tmp.data) mixes.push(item)

      console.log(cds)
    } catch (e) {
      console.log(e)
    }
  }
  loadTmpCd()

  return {
    dumpMix,
    removeMix,
    loadTmpMix,
    mixes,
    cds,
    develop: false
  }
}
