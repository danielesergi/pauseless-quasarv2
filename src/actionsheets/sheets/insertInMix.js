export default function () {
  return {
    grid: false, // visualizza una lista o una griglia (true)
    actions: [
      {
        label: 'Inserisci traccia esterna',
        icon: 'queue_music',
        id: 'insert-song',
        execute: function (emitter, data) {
          emitter.emit('dialog-ext-song')
          // console.log('azione insert-song dentro actionsheet')
        }
      },
      {
        label: 'Inserisci nota',
        icon: 'subject',
        id: 'insert-note',
        execute: function (emitter, data) {
          emitter.emit('dialog-note-song')
        }
      }
    ]
  }
}
