export default function () {
  return {
    grid: false, // visualizza una lista o una griglia (true)
    actions: [
      {
        label: 'Aggiungi a current mix',
        icon: 'queue_music',
        id: 'add-to-mix',
        execute: function (emitter, data) {
          // console.log('emit event')
          emitter.emit('add-to-mix', data)
        }
      },
      {
        label: 'Modifica',
        icon: 'notes',
        id: 'edit-cd-song',
        execute: function (emitter, data) {
          emitter.emit('dialog-cd-song', data)
        }
      }
    ]
  }
}
