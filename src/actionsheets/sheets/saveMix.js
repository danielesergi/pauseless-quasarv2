export default function () {
  return {
    grid: false, // visualizza una lista o una griglia (true)
    actions: [
      {
        label: 'Esporta in temporanei',
        icon: 'save',
        id: 'save-mix-to-temp',
        execute: function (emitter, instance) {
          emitter.emit('save-mix-to-temp')
        }
      }
    ]
  }
}
