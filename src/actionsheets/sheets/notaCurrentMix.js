export default function () {
  return {
    // message: 'Bottom Sheet message',
    grid: false, // visualizza una lista o una griglia (true)
    actions: [
      {
        label: 'Rimuovi dal mix',
        icon: 'delete',
        id: 'remove-from-mix',
        execute: function (emitter, data) {
          emitter.emit('remove-from-mix', data)
        }
      },
      {
        label: 'Posizione',
        icon: 'swap_vert',
        id: 'position-in-mix',
        execute: function (emitter, data) {
          emitter.emit('show-position-dialog', data)
        }
      },
      {
        label: 'Modifica',
        icon: 'notes',
        id: 'notes',
        execute: function (emitter, data) {
          emitter.emit('dialog-note-song', data)
        }
      }
    ]
  }
}
