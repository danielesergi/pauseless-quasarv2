export default function () {
  return {
    grid: false, // visualizza una lista o una griglia (true)
    actions: [
      {
        label: 'Rimuovi dal mix',
        icon: 'delete',
        id: 'remove-from-mix',
        execute: function (emitter, data) {
          emitter.emit('remove-from-mix', data)
        }
      },
      {
        label: 'Posizione',
        icon: 'swap_vert',
        id: 'position-in-mix',
        execute: function (emitter, data) {
          // console.log('position dialog')
          emitter.emit('show-position-dialog')
        }
      },
      {
        label: 'Trova in chiave',
        icon: 'playlist_add',
        id: 'find-in-key',
        execute: function (emitter, data) {
          // console.log('/find-in-key/' + data.$.bpm + '/' + data.$.key.replace('#', '*') + '/')// + v.selectedSongIndex)
          emitter.emit('find-in-key', { bpm: data.$.bpm, key: data.$.key.replace('#', '*') })
        }
      },
      {
        label: 'Sostituisci in chiave',
        icon: 'replay',
        id: 'replace-in-key',
        execute: function (emitter, data) {
          // console.log('/replace-in-key/' + data.$.bpm + '/' + data.$.key.replace('#', '*') + '/')// + v.selectedSongIndex)
          emitter.emit('replace-in-key', { bpm: data.$.bpm, key: data.$.key.replace('#', '*') })
        }
      },
      {
        label: 'Note mix precedenti',
        icon: 'notes',
        id: 'previous-notes',
        execute: function (emitter, data) {
          emitter.emit('previous-notes', data)
        }
      },
      {
        label: 'Note',
        icon: 'notes',
        id: 'notes',
        execute: function (emitter, data) {
          emitter.emit('edit-song-note', data)
        }
      }
    ]
  }
}
