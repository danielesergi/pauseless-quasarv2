export default function () {
  return {
    // message: 'Bottom Sheet message',
    grid: false, // visualizza una lista o una griglia (true)
    actions: [
      {
        label: 'Scarica XML',
        icon: 'cloud_download',
        id: 'download-xml',
        execute: function (emitter) {
          emitter.emit('scarica-xml') // v.scaricaXml()
        }
      },
      {
        label: 'Avanzate...',
        icon: 'more_horiz',
        id: 'advanced' // è una pagina con bottoni di test più che altro, vediamo se tenerla
      },
      {
        label: 'Dark mode',
        icon: 'highlight',
        id: 'dark-toggle',
        execute: function (emitter) {
          emitter.emit('dark-toggle')
        }
      },
      {
        label: 'Settings...',
        icon: 'settings',
        id: 'settings',
        execute: function (emitter) {
          // v.preferencesDialog()
          emitter.emit('show-preferences-dialog')
        }
      }
    ]
  }
}
