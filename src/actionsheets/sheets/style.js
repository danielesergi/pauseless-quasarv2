export default function () {
  return {
    grid: false, // visualizza una lista o una griglia (true)
    actions: [
      {
        label: 'Dark mode',
        icon: 'highlight',
        id: 'dark-toggle',
        execute: function (emitter) {
          emitter.emit('dark-toggle')
        }
      }
    ]
  }
}
