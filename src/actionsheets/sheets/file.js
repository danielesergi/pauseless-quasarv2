export default function () {
  return {
    // message: 'Bottom Sheet message',
    grid: false, // visualizza una lista o una griglia (true)
    actions: [
      {
        label: 'Carica mix',
        icon: 'cloud_upload',
        id: 'load-mix',
        execute: function (emitter, instance) {
          if (!instance.$fileService) console.error('Non sei su mobile!!', instance)
          else {
            instance.$fileService.list().then(
              (l) => {
                const s = []
                console.log(l)
                l.forEach(function (k) {
                  if (k.indexOf('json_data/mix_') === 0) s.push(k.replace('.json', '').replace('json_data/mix_', ''))
                })
                emitter.emit('show-loadmix-dialog', s)
              },
              (e) => { console.error(e) }
            )
          }
        }
      },
      {
        label: 'Cancella mix',
        icon: 'delete',
        id: 'delete-files',
        execute: function (emitter, instance) {
          if (!instance.$fileService) console.error('Non sei su mobile!!')
          else {
            instance.$fileService.list().then(
              (l) => {
                const s = []
                // console.log(l)
                l.forEach(function (k) {
                  if (k.indexOf('json_data/mix_') === 0) s.push(k.replace('.json', '').replace('json_data/mix_', ''))
                })
                // v.simpleListDialog('Cancella mix...', s, 'delete-mix')
                emitter.emit('show-deletemix-dialog', s)
              },
              (e) => { console.error(e) }
            )
          }
        }
      }
    ]
  }
}
