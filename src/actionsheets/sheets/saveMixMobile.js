export default function () {
  return {
    grid: false, // visualizza una lista o una griglia (true)
    actions: [
      {
        label: 'Salva current mix',
        icon: 'save',
        id: 'save-mix',
        execute: function (emitter, instance) {
          emitter.emit('save-mix')
        }
      },
      {
        label: 'Salva current mix as...',
        icon: 'save',
        id: 'save-mix-as',
        execute: function (emitter, instance) {
          // console.log('azione save-mix-as dentro actionsheet', v)
          emitter.emit('show-savemixas-dialog')
        }
      },
      {
        label: 'Invia current mix',
        icon: 'cloud_upload',
        id: 'send-mix',
        execute: function (emitter) {
          // v.sendMixToServer()
          emitter.emit('send-mix')
        }
      },
      {
        label: 'Invia current mix as...',
        icon: 'cloud_upload',
        id: 'send-mix-as',
        execute: function (emitter) {
          // console.log('azione send-mix-as dentro actionsheet')
          emitter.emit('show-sendmixas-dialog')
        }
      }
    ]
  }
}
