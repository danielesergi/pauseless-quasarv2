export default function () {
  return {
    // message: 'Bottom Sheet message',
    grid: false, // visualizza una lista o una griglia (true)
    actions: [
      {
        label: 'Rimuovi dal mix',
        icon: 'delete',
        id: 'remove-from-mix',
        execute: function (emitter, data) {
          emitter.emit('remove-from-mix', data)
        }
      },
      {
        label: 'Posizione',
        icon: 'swap_vert',
        id: 'position-in-mix',
        execute: function (emitter, data) {
          emitter.emit('show-position-dialog', data)
        }
      },
      {
        label: 'Modifica',
        icon: 'notes',
        id: 'edit-ext-song',
        execute: function (emitter, data) {
          emitter.emit('dialog-ext-song', data)
        }
      },
      {
        label: 'Trova in chiave',
        icon: 'playlist_add',
        id: 'find-in-key',
        execute: function (emitter, data) {
          // console.log('/find-in-key/' + v.selectedSong.$.bpm + '/' + v.selectedSong.$.key.replace('#', '*') + '/' + v.selectedSongIndex)
          emitter.emit('find-in-key', { bpm: data.$.bpm, key: data.$.key.replace('#', '*') })
        }
      },
      {
        label: 'Sostituisci in chiave',
        icon: 'replay',
        id: 'replace-in-key',
        execute: function (emitter, data) {
          // console.log('/replace-in-key/' + data.$.bpm + '/' + data.$.key.replace('#', '*') + '/')// + v.selectedSongIndex)
          emitter.emit('replace-in-key', { bpm: data.$.bpm, key: data.$.key.replace('#', '*') })
        }
      },
      {
        label: 'Note',
        icon: 'notes',
        id: 'notes',
        execute: function (emitter, data) {
          emitter.emit('edit-song-note', data)
        }
      }
    ]
  }
}
