export default function () {
  return {
    grid: false, // visualizza una lista o una griglia (true)
    actions: [
      {
        label: 'Cancella current mix',
        icon: 'delete',
        id: 'clear-mix',
        execute: function (emitter) {
          emitter.emit('clear-mix')
        }
      }
      /* ,
      {
        label: 'Aggiorna',
        icon: 'autorenew',
        id: 'refresh',
        execute: function (emitter) {
          console.log('azione refresh: che devo fare???')
        }
      }
      */
    ]
  }
}
