import { BottomSheet } from 'quasar'
import selectSong from './sheets/selectSong'
import saveMix from './sheets/saveMix'
import saveMixMobile from './sheets/saveMixMobile'
import insertInMix from './sheets/insertInMix'
import notaCurrentMix from './sheets/notaCurrentMix'
import songExtCurrentMix from './sheets/songExtCurrentMix'
import songCurrentMix from './sheets/songCurrentMix'
import frontPage from './sheets/frontPage'
import file from './sheets/file'
import currentMixUtils from './sheets/currentMixUtils'
import style from './sheets/style'

const config = {
  currentMixUtils: currentMixUtils(),
  file: file(),
  frontPage: frontPage(),
  song: songCurrentMix(),
  'song-ext': songExtCurrentMix(),
  nota: notaCurrentMix(),
  insertInMix: insertInMix(),
  saveMix: saveMix(),
  saveMixMobile: saveMixMobile(),
  selectSong: selectSong(),
  style: style()
}

export default {
  showSheet: function (c, emitter, data) {
    if (config[c]) {
      BottomSheet.create(config[c]).onOk(action => {
        // console.log('Action chosen:', action)
        // if (action.link) t.$router.push({ path: action.link })
        // else
        if (action.execute) action.execute(emitter, data)
      }).onCancel(() => {
        // console.log('Dismissed')
      }).onDismiss(() => {
        // console.log('I am triggered on both OK and Cancel')
      })
    }
  }
}
