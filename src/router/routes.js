
import { Platform } from 'quasar'
const isMobile = Platform.is.cordova && Platform.is.mobile
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/CdList.vue') },
      { path: '/cd-list', component: () => import('pages/CdList.vue') },
      { path: '/mix-list', component: () => import('pages/MixList.vue') },
      { path: '/current-mix', component: () => import('pages/CurrentMix.vue') },
      { path: '/harmonic', component: () => import('pages/Harmonic.vue') },
      { path: '/tmp-mixes', component: () => import('pages/TmpMixes.vue') },
      { path: '/tmp-cd', component: () => import('pages/TmpCd.vue') },
      { path: '/search', component: () => import('pages/Search.vue') },
      { path: '/avanzate', component: () => import('pages/Avanzate.vue') },
      {
        path: '/cd/:name',
        // component: () => !isMobile ? import('pages/CdForm.vue') : import('pages/Cd.vue'),
        component: () => import('pages/Cd.vue'),
        props: true
      },
      {
        path: '/mix/:name',
        component: () => import('pages/Mix.vue'),
        props: true
      },
      {
        path: '/find-in-key/:bpm/:selectedKey/:idx',
        component: () => import('pages/InKey.vue'),
        props: true
      },
      {
        path: '/replace-in-key/:bpm/:selectedKey/:idx',
        component: () => import('pages/InKey.vue'),
        props: true
      }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
