/// <reference path="cypress" />
/// <reference path="../support/index.d.ts" />

// Use `cy.dataCy` custom command for more robust tests
// See https://docs.cypress.io/guides/references/best-practices.html#Selecting-Elements

// ** This file is an example of how to write Cypress tests, you can safely delete it **

// This test will pass when run against a clean Quasar project
const port = 8080
describe('Landing', () => {
  /* beforeEach(() => {
    cy.visit('/')
  }) */
  it('.should() - assert that <title> is correct', () => {
    cy.visit('/')
    cy.title().should('include', 'PauselessApp')
  })
  it('controlla che la lista dei cd sia stata caricata', () => {
    // cy.get('.q-item').should('have.length', 3) Too many elements found. Found '115', expected '3'.
    cy.get('#cd-list > .q-item').its('length').should('be.gte', 1)
    cy.get('#cd-list > .q-item').should('have.length', 6)

    cy.get('#titolo-pagina').should('contain', 'cd list v5')
  })
})

describe('Menu', () => {
  it('controllo bottoni toolbar in alto', () => {
    cy.get('.q-toolbar > .q-btn').should('have.length', 3)
  })
  it('controlla che il primo cd sia cliccabile e mandi all\' indirizzo corretto', () => {
    cy.get('#cd-list > .q-item:nth-child(1)').click()

    cy.get('#titolo-pagina').should('contain', 'cd da 1')
    cy.url().should('include', 'cd/da%201')
    cy.url().should('eq', 'http://localhost:' + port + '/#/cd/da%201')
  })
  it('clicca sulla prima voce di menu e controlla di tornare alla lista cd', () => {
    cy.get('#menu > .q-item:nth-child(1)').click()

    cy.get('#titolo-pagina').should('contain', 'cd list v5')
    cy.url().should('include', 'cd-list')
    cy.url().should('eq', 'http://localhost:' + port + '/#/cd-list')
  })

  it('clicca sulla seconda voce di menu e controlla di essere sulla lista mix', () => {
    cy.get('#menu > .q-item:nth-child(2)').click()

    cy.get('#titolo-pagina').should('contain', 'mix list')
    cy.url().should('include', 'mix-list')
    cy.url().should('eq', 'http://localhost:' + port + '/#/mix-list')
  })
  it('controllo bottoni toolbar in alto', () => {
    cy.get('.q-toolbar > .q-btn').should('have.length', 2)
  })

  it('clicca sul primo bottone del primo mix', () => {
    cy.get('#mix-list > .q-item').should('have.length', 2)
    // cy.get('#mix-list > .q-item:nth-child(1)').should('include', 'testone')
    cy.get('.bottoni-mix:nth-child(1) > .q-btn:nth-child(1)').first().click()

    cy.get('#titolo-pagina').should('contain', 'testone')
    cy.url().should('include', 'testone')
    cy.url().should('eq', 'http://localhost:' + port + '/#/mix/testone')
  })

  it('torna alla lista mix', () => {
    cy.get('#menu > .q-item:nth-child(2)').click()

    cy.get('#titolo-pagina').should('contain', 'mix list')
    cy.url().should('include', 'mix-list')
    cy.url().should('eq', 'http://localhost:' + port + '/#/mix-list')
  })
  it('clicca sul secondo bottone del primo mix', () => {
    cy.get('.bottoni-mix > .q-btn:nth-child(2)').first().click()

    cy.get('#titolo-pagina').should('contain', 'testone')
    cy.url().should('eq', 'http://localhost:' + port + '/#/current-mix')
  })
  it('controllo bottoni toolbar in alto', () => {
    cy.get('.q-toolbar > .q-btn').should('have.length', 5)
  })

  it('va alla pagina di ricerca', () => {
    cy.get('#menu > .q-item:nth-child(4)').click()

    cy.url().should('include', 'search')
    cy.url().should('eq', 'http://localhost:' + port + '/#/search')
  })
})

describe('Current mix operations (mix di test)', () => {
  it('va alla pagina current mix e controlla ci sia ancora il mix corretto caricato da prima', () => {
    cy.get('#menu > .q-item:nth-child(3)').click()

    cy.get('#titolo-pagina').should('contain', 'testone')
    cy.url().should('eq', 'http://localhost:' + port + '/#/current-mix')

    cy.get('#song-list > .q-item').should('have.length', 20)
  })

  it('clicco sulla prima traccia (local) e controllo il bottom sheet', () => {
    cy.get('#song-list > .q-item:nth-child(1)').click()

    cy.get('.q-bottom-sheet').should('be.visible')
    cy.get('.q-bottom-sheet > div > .q-item').should('have.length', 6)

    cy.get('.q-bottom-sheet > div > .q-item:nth-child(1)').should('contain', 'Rimuovi dal mix')
    cy.get('.q-bottom-sheet > div > .q-item:nth-child(2)').should('contain', 'Posizione')
    cy.get('.q-bottom-sheet > div > .q-item:nth-child(3)').should('contain', 'Trova in chiave')
    cy.get('.q-bottom-sheet > div > .q-item:nth-child(4)').should('contain', 'Sostituisci in chiave')
    cy.get('.q-bottom-sheet > div > .q-item:nth-child(5)').should('contain', 'Note mix precedenti')
    cy.get('.q-bottom-sheet > div > .q-item:nth-child(6)').should('contain', 'Note')

    cy.get('.q-dialog__backdrop').click()
    cy.get('.q-bottom-sheet').should('not.be.visible')
  })

  it('clicco sulla seconda traccia (esterna) e controllo il bottom sheet', () => {
    cy.get('#song-list > .q-item:nth-child(2)').click()

    cy.get('.q-bottom-sheet').should('be.visible')
    cy.get('.q-bottom-sheet > div > .q-item').should('have.length', 6)

    cy.get('.q-bottom-sheet > div > .q-item:nth-child(1)').should('contain', 'Rimuovi dal mix')
    cy.get('.q-bottom-sheet > div > .q-item:nth-child(2)').should('contain', 'Posizione')
    cy.get('.q-bottom-sheet > div > .q-item:nth-child(3)').should('contain', 'Modifica')
    cy.get('.q-bottom-sheet > div > .q-item:nth-child(4)').should('contain', 'Trova in chiave')
    cy.get('.q-bottom-sheet > div > .q-item:nth-child(5)').should('contain', 'Sostituisci in chiave')
    cy.get('.q-bottom-sheet > div > .q-item:nth-child(6)').should('contain', 'Note')

    cy.get('.q-dialog__backdrop').click()
    cy.get('.q-bottom-sheet').should('not.be.visible')
  })

  it('clicco sulla quinta traccia (nota) e controllo il bottom sheet', () => {
    cy.get('#song-list > .q-item:nth-child(5)').click()

    cy.get('.q-bottom-sheet').should('be.visible')
    cy.get('.q-bottom-sheet > div > .q-item').should('have.length', 3)

    cy.get('.q-bottom-sheet > div > .q-item:nth-child(1)').should('contain', 'Rimuovi dal mix')
    cy.get('.q-bottom-sheet > div > .q-item:nth-child(2)').should('contain', 'Posizione')
    cy.get('.q-bottom-sheet > div > .q-item:nth-child(3)').should('contain', 'Modifica')

    cy.get('.q-dialog__backdrop').click()
    cy.get('.q-bottom-sheet').should('not.be.visible')
  })

  it('clicco sulla quinta traccia (nota) e controllo il bottom sheet', () => {
    cy.get('#song-list > .q-item:nth-child(5)').click()

    cy.get('.q-bottom-sheet').should('be.visible')
    cy.get('.q-bottom-sheet > div > .q-item:nth-child(3)').click()

    cy.get('#testo').should('have.value', 'elemento a se stante, porcoddio')
    cy.get('#testo').type('{selectall}')
    cy.get('#testo').type('{backspace}')
    cy.get('#testo').type('dio è molto porco')

    cy.get('#chiudi-modale').click()
    cy.get('#song-list > .q-item:nth-child(5)').should('contain', 'dio è molto porco')
  })

  it('cancello il mix col bottone in alto', () => {
    cy.get('.q-toolbar > button:nth-child(4)').click()

    cy.get('.q-bottom-sheet').should('be.visible')
    cy.get('.q-bottom-sheet > div > .q-item:nth-child(1)').click()

    cy.get('#song-list > .q-item').should('have.length', 0)
  })
})

describe('Current mix operations (mix vuoto)', () => {
  it('entro nel primo cd', () => {
    cy.get('#menu > .q-item:nth-child(1)').click()
    cy.get('#cd-list > .q-item:nth-child(1)').click()

    cy.get('#titolo-pagina').should('contain', 'cd da 1') // solo per sicurezza
  })
  it('seleziono la prima traccia', () => {
    cy.get('#song-list > .q-item:nth-child(1)').click()

    cy.get('.q-bottom-sheet').should('be.visible')
  })
  it('clicco sulla prima voce del bottom sheet', () => {
    cy.get('.q-bottom-sheet > div > .q-item:nth-child(1)').click()

    cy.get('#titolo-pagina').should('contain', 'current mix')
    cy.get('#song-list > .q-item').should('have.length', 1)
  })

  it('entro nel secondo cd', () => {
    cy.get('#menu > .q-item:nth-child(1)').click()
    cy.get('#cd-list > .q-item:nth-child(2)').click()

    cy.get('#titolo-pagina').should('contain', 'cd da 2') // solo per sicurezza
  })
  it('seleziono la seconda traccia', () => {
    cy.get('#song-list > .q-item:nth-child(2)').click()

    cy.get('.q-bottom-sheet').should('be.visible')
  })
  it('clicco sulla prima e unica voce del bottom sheet', () => {
    cy.get('.q-bottom-sheet > div > .q-item:nth-child(1)').click()

    cy.get('#titolo-pagina').should('contain', 'current mix')
    cy.get('#song-list > .q-item').should('have.length', 2)
  })
  it('controllo i nomi e l\'ordine delle due tracce', () => {
    cy.get('#song-list > .q-item:nth-child(1)').should('contain', 'mad maxx')
    cy.get('#song-list > .q-item:nth-child(2)').should('contain', 'alchemix')
  })

  // POSIZIONE
  it('seleziono la prima traccia del mix e apro modale posizione', () => {
    cy.get('#song-list > .q-item:nth-child(1)').click()

    cy.get('.q-bottom-sheet').should('be.visible')
    cy.get('.q-bottom-sheet > div > .q-item:nth-child(2)').click()

    cy.get('#titolo-modale').should('be.visible')
    cy.get('#titolo-modale').should('contain', 'Posizione')
    cy.get('.q-bottom-sheet').should('not.be.visible')
  })
  it('sposto giu la prima traccia', () => {
    cy.get('#muovi-giu').click()

    cy.get('#song-list > .q-item:nth-child(2)').should('contain', 'mad maxx')
    cy.get('#song-list > .q-item:nth-child(1)').should('contain', 'alchemix')
  })
  it('clicco ancora su sposta giu e controllo che l\'ordine non cambi', () => {
    cy.get('#muovi-giu').click()

    cy.get('#song-list > .q-item:nth-child(2)').should('contain', 'mad maxx')
    cy.get('#song-list > .q-item:nth-child(1)').should('contain', 'alchemix')
  })
  it('sposto di nuovo su la traccia', () => {
    cy.get('#muovi-su').click()

    cy.get('#song-list > .q-item:nth-child(1)').should('contain', 'mad maxx')
    cy.get('#song-list > .q-item:nth-child(2)').should('contain', 'alchemix')
  })
  it('clicco ancora su e controllo che l\'ordine non cambi', () => {
    cy.get('#muovi-su').click()

    cy.get('#song-list > .q-item:nth-child(1)').should('contain', 'mad maxx')
    cy.get('#song-list > .q-item:nth-child(2)').should('contain', 'alchemix')
  })
  it('chiudo modale posizione', () => {
    cy.get('#chiudi-modale').click()
    cy.get('#titolo-modale').should('not.be.visible')
  })

  // NOTE
  it('seleziono la prima traccia del mix e apro modale note', () => {
    cy.get('#song-list > .q-item:nth-child(1)').click()

    cy.get('.q-bottom-sheet').should('be.visible')
    cy.get('.q-bottom-sheet > div > .q-item:nth-child(6)').click()

    cy.get('#titolo-modale').should('be.visible')
    cy.get('#titolo-modale').should('contain', 'Salva note')
    cy.get('.q-bottom-sheet').should('not.be.visible')
  })
  it('scrivo nota e chiudo modale', () => {
    cy.get('#testo').type('Nota di test')

    cy.get('#chiudi-modale').click()
    cy.get('#titolo-modale').should('not.be.visible')

    cy.get('#song-list > .q-item:nth-child(1)').should('contain', 'Nota di test')
  })

  it('seleziono la prima traccia del mix e apro modale note mix precedenti', () => {
    cy.get('#song-list > .q-item:nth-child(1)').click()

    cy.get('.q-bottom-sheet').should('be.visible')
    cy.get('.q-bottom-sheet > div > .q-item:nth-child(5)').click()

    cy.get('#titolo-modale').should('be.visible')
    cy.get('#titolo-modale').should('contain', 'Note precedenti')
    cy.get('.q-bottom-sheet').should('not.be.visible')
  })
  it('seleziono la nota precedente', () => {
    cy.get('#lista-opzioni > .q-item:nth-child(1)').click()

    cy.get('#song-list > .q-item:nth-child(1)').should('contain', 'nota su una canzone normalissima')
  })

  it('seleziono la prima traccia del mix e apro modale note', () => {
    cy.get('#song-list > .q-item:nth-child(1)').click()

    cy.get('.q-bottom-sheet').should('be.visible')
    cy.get('.q-bottom-sheet > div > .q-item:nth-child(6)').click()

    cy.get('#titolo-modale').should('be.visible')
    cy.get('#titolo-modale').should('contain', 'Salva note')
    cy.get('.q-bottom-sheet').should('not.be.visible')
  })
  it('cancello nota e chiudo modale', () => {
    cy.get('#testo').clear()

    cy.get('#chiudi-modale').click()
    cy.get('#titolo-modale').should('not.be.visible')

    cy.get('#song-list > .q-item:nth-child(1)').should('not.contain', 'nota su una canzone normalissima')
  })

  // TROVA IN CHIAVE
  it('seleziono la seconda traccia del mix e cerco in chiave', () => {
    cy.get('#song-list > .q-item:nth-child(2)').click()

    cy.get('.q-bottom-sheet').should('be.visible')
    cy.get('.q-bottom-sheet > div > .q-item:nth-child(3)').click()

    cy.url().should('include', 'find-in-key')
    cy.url().should('eq', 'http://localhost:' + port + '/#/find-in-key/141/F*+/1')
  })
  it('prendo una fra le canzoni in chiave', () => {
    cy.get('#item-list-in-key').click()
    cy.get('#song-list-in-key > .q-item:nth-child(3)').click()

    // testare anche che siamo tornati al current mix
    cy.get('#song-list > .q-item').should('have.length', 3)
    cy.get('#song-list > .q-item:nth-child(3)').should('contain', 'avalon')
  })
  it('seleziono la terza traccia del mix e cerco in chiave', () => {
    cy.get('#song-list > .q-item:nth-child(3)').click()

    cy.get('.q-bottom-sheet').should('be.visible')
    cy.get('.q-bottom-sheet > div > .q-item:nth-child(3)').click()

    cy.url().should('include', 'find-in-key')
    cy.url().should('eq', 'http://localhost:' + port + '/#/find-in-key/145/A*+/2')
  })
  it('prendo una fra le canzoni della chiave precedente', () => {
    cy.get('#item-list-key-before').click()
    cy.get('#song-list-key-before > .q-item:nth-child(2)').click()

    cy.get('#song-list > .q-item').should('have.length', 4)
    cy.get('#song-list > .q-item:nth-child(4)').should('contain', 'ital')
  })
  it('seleziono la quarta traccia del mix e cerco in chiave', () => {
    cy.get('#song-list > .q-item:nth-child(4)').click()

    cy.get('.q-bottom-sheet').should('be.visible')
    cy.get('.q-bottom-sheet > div > .q-item:nth-child(3)').click()

    cy.url().should('include', 'find-in-key')
    cy.url().should('eq', 'http://localhost:' + port + '/#/find-in-key/144/G*+/3')
  })
  it('prendo una fra le canzoni della chiave successiva', () => {
    cy.get('#item-list-key-after').click()
    cy.get('#song-list-key-after > .q-item:nth-child(1)').click()

    cy.get('#song-list > .q-item').should('have.length', 5)
    cy.get('#song-list > .q-item:nth-child(5)').should('contain', 'ital')
  })
  it('seleziono la quinta traccia del mix e cerco in chiave', () => {
    cy.get('#song-list > .q-item:nth-child(5)').click()

    cy.get('.q-bottom-sheet').should('be.visible')
    cy.get('.q-bottom-sheet > div > .q-item:nth-child(3)').click()

    cy.url().should('include', 'find-in-key')
    cy.url().should('eq', 'http://localhost:' + port + '/#/find-in-key/143/G*+/4')
  })
  it('prendo una fra le canzoni della chiave relativa', () => {
    cy.get('#item-list-key-relative').click()
    cy.get('#song-list-key-relative > .q-item:nth-child(2)').click()

    cy.get('#song-list > .q-item').should('have.length', 6)
    cy.get('#song-list > .q-item:nth-child(6)').should('contain', 'phase')
  })

  // TODO : replace in key

  it('rimuovo la prima canzone dal mix', () => {
    cy.get('#song-list > .q-item:nth-child(1)').click()

    cy.get('.q-bottom-sheet').should('be.visible')
    cy.get('.q-bottom-sheet > div > .q-item:nth-child(1)').should('be.visible')
    cy.get('.q-bottom-sheet > div > .q-item:nth-child(1)').click()

    cy.get('#song-list > .q-item').should('have.length', 5)
    cy.get('#song-list > .q-item:nth-child(1)').should('contain', 'alchemix')
  })

  it('cancello il mix col bottone in alto', () => {
    cy.get('.q-toolbar > button:nth-child(4)').click()

    cy.get('.q-bottom-sheet').should('be.visible')
    cy.get('.q-bottom-sheet > div > .q-item:nth-child(1)').click()

    cy.get('#song-list > .q-item').should('have.length', 0)
  })
})

// pagina di ricerca ancora non funzionante
/* describe('Cerca', () => {
  it('vado alla pagina di ricerca', () => {
    cy.get('#menu > .q-item:nth-child(4)').click()

    cy.get('#cerca').should('be.visible')
  })

  it('provo una ricerca', () => {
    cy.get('#song-list > .q-item').should('have.length', 20) // lunghezza della lista filtrata di default
    cy.get('#cerca > input').type('avalon')
    cy.get('#song-list > .q-item').should('have.length', 24) // lunghezza lista filtrata per avalon senza limiti
  })

  it('seleziono la prima traccia', () => {
    cy.get('#song-list > .q-item:nth-child(1)').click()

    cy.get('.q-bottom-sheet').should('be.visible')
  })
  it('clicco sulla prima e unica voce del bottom sheet', () => { // per ora gli dico di cliccare sul bottomsheet, unica voce : se ce n'è piu di una?
    cy.get('.q-bottom-sheet > div > .q-item:nth-child(1)').click()

    cy.get('#titolo-pagina').should('contain', 'current mix')
    cy.get('#song-list > .q-item').should('have.length', 1)
  })

  it('cancello il mix col bottone in alto', () => {
    cy.get('.q-toolbar > button:nth-child(4)').click()

    cy.get('.q-bottom-sheet').should('be.visible')
    cy.get('.q-bottom-sheet > div > .q-item:nth-child(1)').click()

    cy.get('#song-list > .q-item').should('have.length', 0)
  })
}) */

// ** The following code is an example to show you how to write some tests for your home page **
//
// describe('Home page tests', () => {
//   beforeEach(() => {
//     cy.visit('/');
//   });
//   it('has pretty background', () => {
//     cy.dataCy('landing-wrapper')
//       .should('have.css', 'background').and('match', /(".+(\/img\/background).+\.png)/);
//   });
//   it('has pretty logo', () => {
//     cy.dataCy('landing-wrapper img')
//       .should('have.class', 'logo-main')
//       .and('have.attr', 'src')
//       .and('match', /^(data:image\/svg\+xml).+/);
//   });
//   it('has very important information', () => {
//     cy.dataCy('instruction-wrapper')
//       .should('contain', 'SETUP INSTRUCTIONS')
//       .and('contain', 'Configure Authentication')
//       .and('contain', 'Database Configuration and CRUD operations')
//       .and('contain', 'Continuous Integration & Continuous Deployment CI/CD');
//   });
// });
