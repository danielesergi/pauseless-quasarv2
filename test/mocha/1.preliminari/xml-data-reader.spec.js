const chai = require('chai')
const expect = chai.expect
const _ = require('underscore')
const fs = require('fs')
// file da testare
const xmlDataReader = require(process.env.PWD + '/server/middleware/xml_data_reader')

// A common pattern for large webapps is to set this as a global in a `setup`
// file, but I'm not doing anything fancy to keep the example simple.

describe('xml_data_reader, operazioni di avvio applicazione con utente test', () => {
  const req = {}

  before(function (done) {
    // runs before all tests in this block
    // prima cosa carico i files con utente test
    xmlDataReader.load(req, {}, function () {
      done()
    }, 'test')
  })

  after(function () {
    fs.rmdir('public/inesistente', { recursive: true, force: true }, () => {
    })
    // rimuove una volta su due
  })
  describe('controlli req.xml_cd_list', () => {
    it('xml_cd_list non è undefined', () => {
      expect(req.xml_cd_list).to.not.be.undefined
    })

    it('xml_cd_list ha una chiave "cds"', (done) => {
      expect(req.xml_cd_list).to.have.keys(['cds'])
      done()
    })

    it('xml_cd_list ha una sola proprietà "cds"', () => {
      expect(req.xml_cd_list).to.have.property('cds')
    })

    it('xml_cd_list di test ha 6 cd', () => {
      expect(_.keys(req.xml_cd_list.cds.cd).length).to.equal(6)
    })
  })

  describe('controlli req.xml_mix_list', () => {
    it('xml_mix_list non è undefined', () => {
      expect(req.xml_mix_list).to.not.be.undefined
    })

    it('xml_mix_list ha una chiave "mixes"', () => {
      expect(req.xml_mix_list).to.have.keys(['mixes'])
    })

    it('xml_mix_list ha una sola proprietà "mixes"', () => {
      expect(req.xml_mix_list).to.have.property('mixes')
    })

    it('xml_mix_list di test ha 1 mix', () => {
      expect(_.keys(req.xml_mix_list.mixes.mix).length).to.equal(2)
    })
  })

  describe('controlli req.v_cd_list', () => {
    it('v_cd_list non è undefined', () => {
      expect(req.v_cd_list).to.not.be.undefined
    })

    it('v_cd_list ha una chiave "cds"', () => {
      expect(req.v_cd_list).to.have.keys(['cds'])
    })

    it('v_cd_list ha una sola proprietà "cds"', () => {
      expect(req.v_cd_list).to.have.property('cds')
    })

    it('v_cd_list di test ha 6 cd', () => {
      expect(_.keys(req.v_cd_list.cds.cd).length).to.equal(6)
    })
  })

  describe('controlli req.v_mix_list', () => {
    it('v_mix_list non è undefined', () => {
      expect(req.v_mix_list).to.not.be.undefined
    })

    it('v_mix_list ha una chiave "mixes"', () => {
      expect(req.v_mix_list).to.have.keys(['mixes'])
    })

    it('v_mix_list ha una sola proprietà "mixes"', () => {
      expect(req.v_mix_list).to.have.property('mixes')
    })

    it('v_mix_list di test ha 1 mix', () => {
      expect(_.keys(req.v_mix_list.mixes.mix).length).to.equal(2)
    })
  })
})

describe('xml_data_reader, operazioni di avvio applicazione con utente inesistente', () => {
  const req = {}

  before(function (done) {
    // prima cosa carico i files con utente test
    xmlDataReader.load(req, {}, function () {
      done()
    }, 'inesistente')
  })

  describe('controlli req.xml_cd_list', () => {
    it('xml_cd_list è settato al valore di default', () => {
      expect(req.xml_cd_list).to.eql({ cds: '' }) // testo contro il valore di default
    })
  })

  describe('controlli req.xml_mix_list', () => {
    it('xml_mix_list è settato al valore di default', () => {
      expect(req.xml_mix_list).to.eql({ mixes: '' }) // testo contro il valore di default
    })
  })

  // TODO : cancellare i files dell'utente inesistente
})
