const chai = require('chai')
const chaiHttp = require('chai-http')
const expect = chai.expect
chai.use(chaiHttp)

const app = require(process.env.PWD + '/server/app')

describe('start applicazione', () => {
  process.env.UTENTE = 'test'

  before(function (done) {
    // runs before all tests in this block
    // app.listen(process.env.PORT || 3000);
    done()
  })

  after(() => {
    console.log('inizio kill di ', app.app.pid)
    process.kill(app.app.pid, 0)
    process.exit(0)
  })

  describe('controlli rotte /json', () => {
    it('get /json/cd è definito e non nullo', (done) => {
      chai.request(app.app)
        .get('/json/cd')
        .end((err, res) => {
          expect(err).to.be.null
          expect(res).to.have.status(200)
          expect(res).to.be.json
          expect(res.body).to.be.a('array')
          expect(res.body.length).to.equal(6)
          done()
        })
    })

    it('get /json/mix è definito e non nullo', (done) => {
      chai.request(app.app)
        .get('/json/mix')
        .end((err, res) => {
          expect(err).to.be.null
          expect(res).to.have.status(200)
          expect(res).to.be.json
          expect(res.body).to.be.a('array')
          expect(res.body.length).to.equal(2)
          done()
        })
    })

    it('get /json/cd/da 1 è definito e non nullo', (done) => {
      chai.request(app.app)
        .get('/json/cd/da%201')
        .end((err, res) => {
          expect(err).to.be.null
          expect(res).to.have.status(200)
          expect(res).to.be.json
          expect(res.body).to.be.a('object')
          expect(res.body.song.length).to.equal(24)
          done()
        })
    })

    it('get /json/mix/testone è definito e non nullo', (done) => {
      chai.request(app.app)
        .get('/json/mix/testone')
        .end((err, res) => {
          expect(err).to.be.null
          expect(res).to.have.status(200)
          expect(res).to.be.json
          expect(res.body).to.be.a('object')
          expect(res.body.song.length).to.equal(20)
          done()
        })
    })
  })
})
