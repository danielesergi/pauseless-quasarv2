const chai = require('chai')
const expect = chai.expect
const xml_data_reader = require(process.env.PWD + '/server/middleware/xml_data_reader')
const _ = require('underscore')

// file da testare
const rk = require(process.env.PWD + '/server/routes/keys')

describe('route keys : get', () => {
  const req = {}
  let response = {}
  const res = {
    status: function (n) { // console.log('mandato stato http', n);
      return this
    },
    send: function (payload) { response = payload }
  }

  describe('controlli get', () => {
    it('get delle keys non è undefined', () => {
      rk.get(req, res)
      expect(response).to.not.be.undefined
    })

    it('get delle keys ha due posizioni (major e minor)', () => {
      expect(_.keys(response).length).to.equal(2)
      expect(response.major).to.not.be.undefined
      expect(response.minor).to.not.be.undefined
    })
  })
})
