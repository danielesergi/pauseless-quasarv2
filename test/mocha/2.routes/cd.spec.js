const chai = require('chai')
const expect = chai.expect
const xml_data_reader = require(process.env.PWD + '/server/middleware/xml_data_reader')
const _ = require('underscore')

// file da testare
const rcd = require(process.env.PWD + '/server/routes/cd')

describe('route cd, operazioni con utente test : get', () => {
  const req = {}
  let response = {}
  const res = {
    status: function (n) { // console.log('mandato stato http', n);
      return this
    },
    send: function (payload) { response = payload }
  }

  before(function (done) {
    // runs before all tests in this block
    // prima cosa carico i files con utente test
    xml_data_reader.load(req, {}, function () {
      rcd.get(req, res)
      done()
    }, 'test')
  })

  describe('controlli get', () => {
    it('get di tutti i cd non è undefined', () => {
      expect(response).to.not.be.undefined
    })

    it('get di tutti i cd ha 6 cd con utente test', () => {
      expect(response.length).to.equal(6)
    })
  })
})

describe('route cd, operazioni con utente test : get_cd', () => {
  const req = {}
  let response = {}
  const res = {
    status: function (n) { // console.log('mandato stato http', n);
      return this
    },
    send: function (payload) { response = payload }
  }

  before(function (done) {
    // runs before all tests in this block
    // prima cosa carico i files con utente test
    xml_data_reader.load(req, {}, function () {
      req.params = { id: 'da 1' }
      rcd.get_cd(req, res)
      done()
    }, 'test')
  })

  describe('controlli get', () => {
    it('get di cd "da 1" non è undefined', () => {
      expect(response).to.not.be.undefined
    })

    it('get di cd "da 1" ha 24 canzoni con utente test', () => {
      expect(response.song.length).to.equal(24)
    })
  })
})

describe('route cd, operazioni con utente test : get_cd_xml', () => {
  const req = {}
  let response = {}
  const res = {
    status: function (n) { // console.log('mandato stato http', n);
      return this
    },
    send: function (payload) { response = payload }
  }

  before(function (done) {
    // runs before all tests in this block
    // prima cosa carico i files con utente test
    xml_data_reader.load(req, {}, function () {
      req.params = { id: 'da 1' }
      rcd.get_cd_xml(req, res)
      done()
    }, 'test')
  })

  describe('controlli get', () => {
    it('get_cd_xml di cd "da 1" non è undefined', () => {
      expect(response).to.not.be.undefined
    })

    it('get_cd_xml di cd "da 1" inizia per <cd name="da 1">', () => {
      expect(response.indexOf('<cd name="da 1">')).to.equal(0)
    })
  })
})

describe('route cd, operazioni con utente test : add_cd', () => {
  const req = {}
  let response = {}
  const res = {
    status: function (n) { // console.log('mandato stato http', n);
      return this
    },
    send: function (payload) { response = JSON.parse(JSON.stringify(payload)) }
  }

  before(function (done) {
    // runs before all tests in this block
    // prima cosa carico i files con utente test
    xml_data_reader.load(req, {}, function () {
      done()
    }, 'test')
  })

  describe('controlli e manipolazione lista cd', () => {
    it('get di tutti i cd non è undefined', () => {
      rcd.get(req, res)
      expect(response).to.not.be.undefined
    })

    it('get di tutti i cd ha 6 cd con utente test', () => {
      expect(response.length).to.equal(6)
    })

    it('aggiungo cd con utente test', () => {
      req.body = JSON.parse(JSON.stringify(exampleCd))
      req.body.song = []
      rcd.add_cd(req, res)
      expect(response.length).to.equal(7)
    })

    it('aggiungo canzone ad un cd esistente con utente test', () => {
      req.params = { id: 'da 7' }
      rcd.get_cd(req, res)
      expect(response.song.length).to.equal(0)

      req.body = JSON.parse(JSON.stringify(exampleCd))
      delete req.body.song[0].$.track // la canzone la mando senza numero di traccia
      req.body.song[0].$.artist = 'artista'
      req.body.song[0].$.title = 'titolo'
      // req.body = {'$':{'name':'da 7'},'song':[{'$':{'artist':'artista','track':1,'title':'titolo','wav':'wav','bpm':'120','key':''},'pause':['-']}]}
      rcd.add_song(req, res)
      expect(response.song.length).to.equal(1)
    })

    it('controllo numerazione automatica canzoni con utente test', () => {
      req.body = JSON.parse(JSON.stringify(exampleCd))
      // modifico il body per adattarsi all'interfaccia
      delete req.body.song[0].$.track // la canzone la mando senza numero di traccia
      req.body.song[0].$.artist = 'artista due'
      req.body.song[0].$.title = 'titolo due'
      rcd.add_song(req, res)

      expect(response.song.length).to.equal(2)
      expect(response.song[0].$.track).to.equal(1)
      expect(response.song[1].$.track).to.equal(2)
    })

    it('modifica traccia con utente test', () => {
      req.params = { id: 'da 7' }
      rcd.get_cd(req, res)
      expect(response.song[0].$.artist).to.equal('artista')
      expect(response.song[0].$.title).to.equal('titolo')

      // modifico il body per adattarsi all'interfaccia
      req.body = JSON.parse(JSON.stringify(exampleCd))
      req.body.song[0].$.track = 1 // l'interfaccia richiede il numero della traccia
      req.body.song[0].$.artist = 'artista modificato'
      req.body.song[0].$.title = 'titolo modificato'
      req.body.cd = req.body.$.name
      req.body.song = req.body.song[0]
      rcd.update_song(req, res)

      expect(response.song[0].$.artist).to.equal('artista modificato')
      expect(response.song[0].$.title).to.equal('titolo modificato')
    })

    it('swappo traccia 1 e traccia 2', () => {
      req.body = { cd: 'da 7', from: 1, to: 2 }
      rcd.swap_song(req, res)

      expect(response.song[1].$.artist).to.equal('artista modificato')
      expect(response.song[1].$.title).to.equal('titolo modificato')
      expect(response.song[0].$.artist).to.equal('artista due')
      expect(response.song[0].$.title).to.equal('titolo due')
      expect(response.song[0].$.track).to.equal(1)
      expect(response.song[1].$.track).to.equal(2)
    })

    it('rimuovo la prima traccia', () => {
      req.body = { cd: 'da 7', song: { $: { track: 1 } } }
      rcd.delete_song(req, res)

      expect(response.song.length).to.equal(1)
      expect(response.song[0].$.track).to.equal(1)
      expect(response.song[0].$.artist).to.equal('artista modificato')
      expect(response.song[0].$.title).to.equal('titolo modificato')
    })
  })
})

const exampleSong = {
  $: {
    track: 1,
    bpm: 140,
    artist: 'mad maxx',
    wav: 'wav',
    key: 'B+',
    title: 'B+ 140',
    used: 1,
    used_mixes: ['testone']
  },
  pause: ['3.20-2.00']
}

const exampleCd = {
  $:
    { name: 'da 7' },
  song: [
    exampleSong
  ]
}
