const chai = require('chai')
const expect = chai.expect
const xml_data_reader = require(process.env.PWD + '/server/middleware/xml_data_reader')
const _ = require('underscore')

// file da testare
const rsong = require(process.env.PWD + '/server/routes/song')

describe('route song, operazioni con utente test : get', () => {
  const req = {}
  let response = {}
  const res = {
    status: function (n) { console.log('mandato stato http', n); return this },
    send: function (payload) { response = payload }
  }

  before(function (done) {
    // runs before all tests in this block
    // prima cosa carico i files con utente test
    xml_data_reader.load(req, {}, function () {
      done()
    }, 'test')
  })

  describe('controlli all', () => {
    it('get di tutte le canzoni non è undefined', () => {
      rsong.all(req, res)
      // console.log(JSON.stringify(response))
      expect(response).to.not.be.undefined
      expect(response.length).to.equal(144)// il file di test contiene 24*6 canzoni
    })
  })

  describe('controlli get', () => {
    it('get di una canzone non è undefined', () => {
      req.params = { cd: 'da 1', track: 10 }
      rsong.get(req, res)
      console.log(JSON.stringify(response))
      expect(response).to.not.be.undefined
      expect(response.$.track).to.equal('10')// controllo che sia la traccia giusta
      expect(response.$.path).to.equal(req.params.cd + '/' + req.params.track)
    })

    it('get di una canzone con cd inesistente è un oggetto vuoto', () => {
      req.params = { cd: 'da 666', track: 10 }
      rsong.get(req, res)
      // console.log(JSON.stringify(response))
      expect(response).to.be.deep.equal({})
    })

    it('get di una canzone con traccia inesistente è un oggetto vuoto', () => {
      req.params = { cd: 'da 1', track: 1000 }
      rsong.get(req, res)
      // console.log(JSON.stringify(response))
      expect(response).to.be.deep.equal({})
    })
  })

  describe('controlli artist', () => {
    it('get della lista artisti presenti non è null e conta 10 posizioni', () => {
      rsong.artist(req, res)
      console.log(JSON.stringify(response))
      expect(response).to.not.be.undefined
      expect(response.length).to.equal(10)// il file di test contiene 10 autori
    })
  })
})
