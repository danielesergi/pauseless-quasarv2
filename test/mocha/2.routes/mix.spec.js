const chai = require('chai')
const expect = chai.expect
const xml_data_reader = require(process.env.PWD + '/server/middleware/xml_data_reader')
const _ = require('underscore')

// file da testare
const rmix = require(process.env.PWD + '/server/routes/mix')

describe('route mix, operazioni con utente test : get', () => {
  const req = {}
  let response = {}
  const res = {
    status: function (n) { console.log('mandato stato http', n); return this },
    send: function (payload) { response = payload }
  }

  before(function (done) {
    // runs before all tests in this block
    // prima cosa carico i files con utente test
    xml_data_reader.load(req, {}, function () {
      done()
    }, 'test')
  })

  describe('controlli get', () => {
    it('get di tutti i mix non è undefined', () => {
      rmix.get(req, res)
      // console.log(JSON.stringify(response))
      expect(response).to.not.be.undefined
    })

    it('get di tutti i mix ha 1 mix con utente test', () => {
      // console.log(JSON.stringify(response))
      expect(response.length).to.equal(2)
    })
  })

  describe('controlli formato canzoni', () => {
    it('controllo formato canzoni tipo local', () => {
      for (const song of response[0].song) {
        if (!song.$.type || song.$.type === 'local') {
          expect(song.$).to.have.keys(['path', 'note'])
        }
      }
    })

    it('controllo formato canzoni tipo ext', () => {
      for (const song of response[0].song) {
        if (song.$.type && song.$.type === 'ext') {
        // {"type":"ext","title":"batte","artist":"tua madre","bpm":"144","key":"B+","note":"non metterla fa cagare"}
          expect(song.$).to.have.keys(['type', 'title', 'artist', 'bpm', 'key', 'note'])
        }
      }
    })

    it('controllo formato note', () => {
      for (const song of response[0].song) {
        if (song.$.type && song.$.type === 'nota') {
        // {"type":"nota","note":"elemento a se stante, porcoddio"}
          expect(song.$).to.have.keys(['type', 'note'])
        }
      }
    })
  })
})

describe('route mix, operazioni con utente test : get_mix', () => {
  const req = {}
  let response = {}
  const res = {
    status: function (n) { console.log('mandato stato http', n); return this },
    send: function (payload) { response = payload }
  }

  before(function (done) {
    // runs before all tests in this block
    // prima cosa carico i files con utente test
    xml_data_reader.load(req, {}, function () {
      done()
    }, 'test')
  })

  describe('controlli get', () => {
    it('get del mix di test non è undefined', () => {
      req.params = { id: 'testone' }
      rmix.get_mix(req, res)
      // console.log(JSON.stringify(response))
      expect(response).to.not.be.undefined
    })

    /* it('get di tutti i mix ha 1 mix con utente test', () => {
            console.log(JSON.stringify(response))
            expect(response.length).to.equal(1);
        }); */
  })

  describe('controlli formato canzoni', () => {
    it('controllo formato canzoni tipo local', () => {
      for (const song of response.song) {
        if (!song.$.type || song.$.type === 'local') {
          expect(song.$).to.have.keys(['path', 'note', 'bpm', 'key', 'song'])
        }
      }
    })

    it('controllo formato canzoni tipo ext', () => {
      for (const song of response.song) {
        if (song.$.type && song.$.type === 'ext') {
        // {"type":"ext","title":"batte","artist":"tua madre","bpm":"144","key":"B+","note":"non metterla fa cagare","song":"tua madre batte (144)"}
          expect(song.$).to.have.keys(['type', 'title', 'artist', 'bpm', 'key', 'note', 'song'])
          expect(song.$.song).to.equal(song.$.artist + ' ' + song.$.title + ' (' + song.$.bpm + ')')
        }
      }
    })

    it('controllo formato note', () => {
      for (const song of response.song) {
        if (song.$.type && song.$.type === 'nota') {
        // {"type":"nota","note":"elemento a se stante, porcoddio"}
          expect(song.$).to.have.keys(['type', 'note'])
        }
      }
    })
    // .to.have.property(‘b’, 2); testare valore
    // .to.have.property(‘b’); testare solo proprieta
  })
})

describe('route mix, operazioni con utente test : get_tmp_mixes', () => {
  let req = { body: {} }
  let response = {}
  const res = {
    status: function (n) { console.log('mandato stato http', n); return this },
    send: function (payload) { response = payload; if (this._done) this._done() }
  }

  before(function (done) {
    // runs before all tests in this block
    // prima cosa carico i files con utente test
    xml_data_reader.load(req, {}, function () {
      done()
    }, 'test')
  })

  describe('controlli get_tmp_mixes con utente test', () => {
    it('get della directory dei mix temporanei con utente test non è undefined', (done) => {
      res._done = function () {
        // console.log(JSON.stringify(response))
        expect(response).to.not.be.undefined
        done()
      }
      rmix.get_tmp_mixes(req, res)
    })
  })

  describe('controlli get_tmp_mixes con utente test', () => {
    it('get della directory dei mix temporanei ha 0 mix', (done) => {
      res._done = function () {
        // console.log('risposta:', JSON.stringify(response))
        expect(response).to.not.be.undefined
        expect(response.length).to.equal(0)
        done()
      }
      rmix.get_tmp_mixes(req, res)
    })

    it('aggiunta di un mix', (done) => {
      res._done = function () {
        // console.log(JSON.stringify(response))
        done()
      }
      req.body.mix = 'temp_mix_di_test'
      req.body.xml = testMix
      rmix.save_json_mix(req, res)
    })

    it('get della directory dei mix temporanei con utente test non è undefined e ora ha un mix', (done) => {
      res._done = function () {
        // console.log(JSON.stringify(response))
        expect(response).to.not.be.undefined
        expect(response.length).to.equal(1)
        done()
      }
      rmix.get_tmp_mixes(req, res)
    })

    it('rimozione di un mix', (done) => {
      res._done = function () {
        // console.log(JSON.stringify(response))
        done()
      }
      req = { g_user: 'test', params: { name: 'tmp_temp_mix_di_test' } }
      rmix.remove_tmp_mix(req, res)
    })

    it('get della directory dei mix temporanei ha di nuovo 0 mix dopo la rimozione del mix di test', (done) => {
      res._done = function () {
        // console.log('risposta:', JSON.stringify(response))
        expect(response).to.not.be.undefined
        expect(response.length).to.equal(0)
        done()
      }
      rmix.get_tmp_mixes(req, res)
    })
  })
})

const testMix = {
  $:
{ name: 'temp_mix_di_test', description: '', bpm: '100' },
  song: [
    { $: { path: 'da 2/1', note: 'local test' } },
    { $: { type: 'ext', title: 'di test', artist: 'canzone', bpm: '100', key: 'A+', note: 'nota di test' } },
    { $: { type: 'nota', note: 'nota di una nota di test' } }
  ]
}
