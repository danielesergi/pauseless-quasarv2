/* eslint-disable camelcase */
const express = require('express')
const bodyParser = require('body-parser') // middleware per risposte in json
const path = require('path')
const cd = require('./routes/cd')
const mix = require('./routes/mix')
const keys = require('./routes/keys')
const song = require('./routes/song')
const xml_data_reader = require('./middleware/xml_data_reader')
const cors = require('cors')
const { promisify } = require('util')
const formidable = require('formidable')
const fs = require('fs')

let initCallback
let xml_cd_list, xml_mix_list, v_cd_list, v_mix_list, v_used_songs = {}

const app = express()
// log.info('🚀 SERVER started: env %s, config %j', process.env.NODE_ENV, CONFIG)
console.info('🚀 SERVER started on port %s', process.env.PORT || 3000)

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cors()) // cross site origin

require('dns').lookup(require('os').hostname(), function (err, add, fam) {
  if (err) console.log(err) // throw err
  else console.log('[INFO] IP addr supposto: ' + add)
})

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS')
  // res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
  res.header('Access-Control-Allow-Headers', 'Content-Type,x-prototype-version,x-requested-with')
  next()
})

app.use(function (req, res, next) {
  if (!req.v_cd_list) req.v_cd_list = v_cd_list
  if (!req.v_mix_list) req.v_mix_list = v_mix_list
  if (!req.xml_cd_list) req.xml_cd_list = xml_cd_list
  if (!req.xml_mix_list) req.xml_mix_list = xml_mix_list
  if (!req.v_used_songs) req.v_used_songs = v_used_songs

  next()
})

app.use(function (req, res, next) {
  // console.log('argomenti funzione', process.argv)
  req.g_user = process.env.UTENTE// process.argv[2]
  xml_data_reader.load(req, res, next, req.g_user)
})

app.use(function (req, res, next) {
  if (req.v_cd_list) v_cd_list = req.v_cd_list
  if (req.v_mix_list) v_mix_list = req.v_mix_list
  if (req.xml_cd_list) xml_cd_list = req.xml_cd_list
  if (req.xml_mix_list) xml_mix_list = req.xml_mix_list
  if (req.v_used_songs) v_used_songs = req.v_used_songs

  next()
})

app.get('/test', async (req, res, next) => {
  try {
    res.status(200).send({ ok: 1 })
  } catch (err) {
    return next({ message: err })
  }
})

app.get('/json/cd', cd.get)
app.get('/json/mix', mix.get)
app.get('/json/cd/:id', cd.get_cd)
app.get('/json/mix/:id', mix.get_mix)
app.get('/json/tmpmix', mix.get_tmp_mixes)
app.get('/json/keys', keys.get)
app.get('/json/songs', song.all)
app.get('/json/artists', song.artist)
app.post('/json/mix', mix.save_json_mix)// questa forma di url mi piace di piu...se abbandono la vecchia app, cambiarle cosi
app.get('/json/tmpcd', cd.get_tmp_cds)

app.get('/tmpmix/save/:name', mix.save_tmp_mix)
app.get('/tmpmix/remove/:name', mix.remove_tmp_mix)
app.get('/xml/cd/:id', cd.get_cd_xml)
app.post('/mix/post', mix.save_mix)
app.post('/cd/add', cd.add_cd)
app.post('/cd/post', cd.add_cd)
app.post('/cd/dump', cd.dump_cds)
app.post('/mix/dump', mix.dump_mixes)
app.post('/cd/song/add', cd.add_song)
app.post('/cd/song/update', cd.update_song)
app.post('/cd/song/delete', cd.delete_song)
app.post('/cd/song/swap', cd.swap_song)
app.get('/song/:cd/:track', song.get)
app.get('/txt/cd/:from/:to', cd.get_range_txt)
app.post('/upload', (req, res) => {
  const form = new formidable.IncomingForm()
  const folder = path.join(__dirname, '../public/' + req.g_user + '/cdfiles')
  if (!fs.existsSync(folder)) {
    fs.mkdirSync(folder)
  }

  form.uploadDir = folder
  form.parse(req, (_, fields, files) => {
    console.log('\n-----------')
    console.log('Fields', fields)
    console.log('Received:', Object.keys(files))
    console.log()
    res.send('Thank you')
  })
  form.on('file', function (field, file) {
    // rename the incoming file to the file's name
    fs.renameSync(file.filepath, form.uploadDir + '/' + file.originalFilename)
  })
})

app.use('/public', express.static(path.join(__dirname, '/public')))

// app.listen(process.env.PORT || 3000)
// module.exports = app

const server = app.listen(process.env.PORT || 3000, function () {
  if (initCallback) initCallback()
})
server.timeout = 1000000

async function stopHTTPServer () {
  console.log('Closing HTTP Server')
  await promisify(server.close.bind(server))()
  console.log('HTTP Server stopped')
}

process.on('SIGTERM', async () => {
  console.info('SIGTERM signal received')
  await stopHTTPServer()
  process.exit(0)
})
process.on('SIGINT', async () => {
  console.info('SIGINT signal received')
  await stopHTTPServer()
  process.exit(0)
})

// per i test
app.pid = process.pid
module.exports = {
  app,
  init: function (cb) {
    initCallback = cb
    return app
  }
}
