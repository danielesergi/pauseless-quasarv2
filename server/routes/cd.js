/* eslint-disable camelcase */
const _ = require('lodash'),
  xml2js = require('xml2js'),
  fs = require('fs'),
  JsDiff = require('diff')

let v_cd_list// , v_used_songs

function get_cd (name) {
  // console.log(v_cd_list)
  for (const cd of v_cd_list.cds.cd) {
    if (cd.$.name === name) {
      // console.log(v_cd_list.cds.cd[key]);
      return (cd)
    }
  }
  return ([])
  // return({ '$': { 'name': name } });
}

exports.get_range_txt = function (req, res) {
  const from = req.params.from, to = req.params.to, ret = []
  let isInRange = false
  for (const cd of v_cd_list.cds.cd) {
    if (cd.$.name === from) {
      isInRange = true
    }

    if (isInRange) ret.push(cd)

    if (cd.$.name === to) {
      isInRange = false
    }
  }
  res.status(200).send(ret)
}

exports.get = function (req, res) {
  try {
    if (req.v_cd_list.cds.cd) res.status(200).send(req.v_cd_list.cds.cd)
    else res.status(200).send([])
  } catch (e) {
    res.status(200).send([])// non esistono cd
  }
}

exports.get_cd = function (req, res) {
  // non so quando questa funzione venga effettivamente usata, comunque il calcolo degli used_mixes l'ho spostato al caricamento dell'xml
  v_cd_list = req.v_cd_list
  const v_used_songs = req.v_used_songs,
    id = req.params.id,
    cd = get_cd(id)

  if (cd.song) {
    _.each(cd.song, function (song) {
      if (v_used_songs[id + '/' + song.$.track]) {
        song.$.used = v_used_songs[id + '/' + song.$.track].length
        song.$.used_mixes = v_used_songs[id + '/' + song.$.track]
      } else song.$.used = 0
    })
    // console.log('cd', cd);
  } else cd.song = []

  res.status(200).send(cd)
}

exports.get_cd_xml = function (req, res) {
  v_cd_list = req.v_cd_list
  const cd = get_cd(req.params.id)

  const builder = new xml2js.Builder({ rootName: 'cd', headless: true })
  const xml = builder.buildObject(cd)
  res.status(200).send(xml)
}

exports.add_cd = function (req, res) {
  if (!req.v_cd_list.cds.cd) req.v_cd_list.cds.cd = []

  req.v_cd_list.cds.cd.push(req.body)
  res.status(200).send(req.v_cd_list.cds.cd)
}

exports.add_song = function (req, res) {
  v_cd_list = req.v_cd_list
  const cd = get_cd(req.body.$.name)

  // if (!req.v_cd_list.cds.cd[cdk].song) req.v_cd_list.cds.cd[cdk].song = []
  if (!cd.song) cd.song = []

  for (const i in req.body.song) {
    req.body.song[i].$.track = cd.song.length + 1// numerazione automatica
    cd.song.push(req.body.song[i])
  }
  rebuild_track_order(cd.song)

  res.status(200).send(cd)
}

exports.update_song = function (req, res) {
  v_cd_list = req.v_cd_list
  const cd = get_cd(req.body.cd)

  // req.body.song.$ = _.pick(req.body.song.$, 'artist', 'track', 'title', 'wav', 'bpm', 'key')
  // req.body.song.pause = []
  // req.body.song.pause.push('-')
  cd.song[parseInt(req.body.song.$.track) - 1] = req.body.song

  // console.log('risultato update song', JSON.stringify(get_cd(req.body.cd)))
  res.status(200).send(cd)
}

function rebuild_track_order (songs) {
  for (const s in songs) songs[s].$.track = parseInt(s) + 1
}

exports.delete_song = function (req, res) {
  v_cd_list = req.v_cd_list
  const cd = get_cd(req.body.cd)
  // console.log(req.v_cd_list.cds.cd[cdk].song)

  cd.song.splice(parseInt(req.body.song.$.track) - 1, 1)
  rebuild_track_order(cd.song)
  // console.log(req.v_cd_list.cds.cd[cdk].song)
  res.status(200).send(cd)
}

exports.swap_song = function (req, res) {
  v_cd_list = req.v_cd_list
  const cd = get_cd(req.body.cd)
  const pos1 = parseInt(req.body.from) - 1, pos2 = parseInt(req.body.to) - 1
  // console.log(req.v_cd_list.cds.cd[cdk].song)
  const tmp = cd.song[pos2]
  cd.song[pos2] = cd.song[pos1]
  cd.song[pos1] = tmp
  // req.v_cd_list.cds.cd[cdk].song[pos1] = req.v_cd_list.cds.cd[cdk].song.splice(pos2, 1, req.v_cd_list.cds.cd[cdk].song[pos1])[0]
  // console.log(req.v_cd_list.cds.cd[cdk].song)
  rebuild_track_order(cd.song)
  res.status(200).send(cd)
}

function getFormattedTime () {
  const today = new Date()
  const y = today.getFullYear()
  // JavaScript months are 0-based.
  const m = today.getMonth() + 1
  const d = today.getDate()
  const h = today.getHours()
  const mi = today.getMinutes()
  const s = today.getSeconds()
  return y + '-' + m + '-' + d + '-' + h + '-' + mi + '-' + s
}

const createCdDiff = function (user, cd_list) {
  return new Promise((resolve, reject) => {
    fs.readFile('./public/' + user + '/cd_list.xml', function (err, data) {
      if (err) {
        console.error(err)
      }

      if (data) {
        try {
          resolve(JsDiff.createPatch('./public/' + user + '/cd_list.xml', data.toString(), cd_list.toString()))
        } catch (e) {
          console.error('errore 1', e)
          reject(e)
        }
      } else {
        console.log('[ERROR] createCdDiff nessun dato trovato per cd_list.xml per ' + user)
      }
    })
  })
}

exports.dump_cds = async function (req, res) {
  const builder = new xml2js.Builder({ renderOpts: { pretty: true } })
  function _clean (dirty) {
    for (const i in dirty.cds.cd) {
      for (const y in dirty.cds.cd[i]) {
        for (const z in dirty.cds.cd[i][y]) {
          if (dirty.cds.cd[i][y][z].$) { // è una canzone
            // console.log("a", dirty.cds.cd[i][y][z].$.used)
            dirty.cds.cd[i][y][z].$ = _.pick(dirty.cds.cd[i][y][z].$, 'track', 'bpm', 'artist', 'wav', 'key', 'title')
          // else //è un cd
          // console.log("b", dirty.cds.cd[i][y][z])
          }
        }
      }
    }
    return dirty
  }
  const xml = builder.buildObject(_clean(req.v_cd_list))
  // console.log(xml)
  const diff = await createCdDiff(req.g_user, xml)

  fs.writeFile('./public/' + req.g_user + '/cd_list.xml', xml, function (err) {
    if (err) throw err
    console.log('[INFO] file cd salvato!')
    fs.writeFile('./public/' + req.g_user + '/cd_diff' + getFormattedTime() + '.diff', diff, function (err) {
      if (err) throw err
    })
    res.status(200).send({})
  })
}

exports.get_tmp_cds = function (req, res) {
  fs.readdir('./public/' + req.g_user + '/cdfiles/', function (err, files) {
    if (err) {
      res.status(200).send([])
    } else {
      console.log('[INFO]', files)
      res.status(200).send(files)
    }
  })
}
