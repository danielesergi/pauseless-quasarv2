/* eslint-disable camelcase */
const _ = require('lodash')

exports.get = function (req, res) {
  // console.log('song.get', req.params.cd, req.params.track, req.v_cd_list.cds.cd[0].$.name)

  const v_cd_list = _.clone(req.v_cd_list)// , v_used_songs = req.v_used_songs
  let cd = _.find(v_cd_list.cds.cd, (el) => { return el.$.name === req.params.cd })

  if (cd && cd.song) {
    cd = cd.song
    const song = _.find(cd, (el) => { return el.$.track === req.params.track.toString() })
    if (song) res.status(200).send(song)
    else res.status(200).send({})
  } else res.status(200).send({})
}

exports.all = function (req, res) {
  const all_cds = _.clone(req.v_cd_list.cds.cd)
  // all_cds = [all_cds[0], all_cds[1]]
  const canzoni = _.flatten(_.map(all_cds, function (cd) {
    const ret = _.clone(cd.song)
    const result = []
    _.each(ret, function (canzone) {
      canzone.$.cd = cd.$.name
      canzone.$.path = cd.$.name + '/' + canzone.$.track
      canzone.$.pause = canzone.pause
      delete canzone.pause
      result.push(canzone.$)
    })
    return result
  }))
  // console.log(canzoni)
  res.status(200).send(canzoni)
}

exports.artist = function (req, res) {
  const all_cds = _.clone(req.v_cd_list.cds.cd)
  // all_cds = [all_cds[0], all_cds[1]]
  const canzoni = _.flatten(_.map(all_cds, function (cd) {
    const ret = _.clone(cd.song)
    const result = []
    _.each(ret, function (canzone) {
      result.push(canzone.$.artist)
    })
    return result
  }))
  // console.log(canzoni)
  res.status(200).send(_.uniq(canzoni).sort())
}
