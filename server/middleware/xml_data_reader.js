/* eslint-disable camelcase */
const xml2js = require('xml2js'),
  fs = require('fs'),
  q = require('q')

let g_user, xml_cd_list, xml_mix_list, v_cd_list, v_mix_list
const v_used_songs = {}

console.log('[INFO] inizializzazione xml_data_reader')
const carica_xml = function (name) {
  const deferred = q.defer()

  const parser = new xml2js.Parser()

  fs.readFile('./public/' + g_user + '/' + name + '.xml', function (err, data) {
    if (err) throw err

    if (data) {
      parser.parseString(data, function (err, result) {
        if (err) { console.log(err); deferred.reject(err) }

        console.log('[INFO] caricato ' + name + ' per ' + g_user)
        deferred.resolve(result)
      })
    } else {
      deferred.reject('Nessun dato caricato per ')
      console.log('[ERROR] nessun dato trovato per ' + name + ' per ' + g_user)
    }
  })

  return deferred.promise
}

const carica_mix = function () {
  const deferred = q.defer()

  const song_near = {}

  carica_xml('mix_list')
    .then(function (mix_list) {
      xml_mix_list = mix_list

      if (mix_list.mixes === '') {
        v_mix_list = { mixes: { mix: [] } }
        deferred.resolve()// non ci sono mix : dovrebbe succedere solo in caso di nuovo file
      }

      console.log('[INFO] mix_list.xml caricato. ' + mix_list.mixes.mix.length)
      v_mix_list = mix_list
      // console.log('[INFO] post caricamento')

      for (const key in v_mix_list.mixes.mix) {
        // console.log('carica_xml1', v_mix_list.mixes.mix[key])
        const songs = v_mix_list.mixes.mix[key].song

        let song_prec
        for (const key2 in songs) {
          // console.log('carica_xml2', key2)
          // inizializzazione struttura dati per catalogare le canzoni gia usate di seguito in un mix
          if (song_prec) {
            if (!song_near[songs[key2].$.path + '-' + song_prec]) {
              song_near[songs[key2].$.path + '-' + song_prec] = 1
            } else {
              song_near[songs[key2].$.path + '-' + song_prec]++
            }
          }
          try {
            // console.log('carica_xml3', key2, v_used_songs, songs[key2].$.path)
          } catch (e) {
            console.log('errore', e)
          }
          // inizializzazione struttura dati per catalogare le canzoni gia usate nei vari mix
          if (!v_used_songs[songs[key2].$.path]) {
            v_used_songs[songs[key2].$.path] = []
            v_used_songs[songs[key2].$.path].push(v_mix_list.mixes.mix[key].$.name)
          } else {
            v_used_songs[songs[key2].$.path].push(v_mix_list.mixes.mix[key].$.name)
          }

          song_prec = songs[key2].$.path
          // console.log('carica_xml4', key2)
        }
        // console.log('carica_xml5', key)
      }
      console.log('[INFO] post song near')

      deferred.resolve()

      // console.log('carica_xml2', _.find(song_near, function (el) {
      // }))
    })

  return deferred.promise
}

const carica_cd = function () {
  console.log('[INFO] inizio caricamento cd')
  const deferred = q.defer()

  carica_xml('cd_list')
    .then(function (cd_list) {
      xml_cd_list = cd_list

      if (cd_list.cds === '') {
        v_cd_list = { cds: { cd: [] } }
        deferred.resolve()// non ci sono cd : dovrebbe succedere solo in caso di nuovo file
      }

      console.log('[INFO] cd_list.xml caricato. ' + cd_list.cds.cd.length)
      v_cd_list = cd_list

      for (const i in v_cd_list.cds.cd) {
        const cd = v_cd_list.cds.cd[i]
        // console.log(cd)
        // aggiungo alle varie canzoni dei cd i loro usi nei mix precedenti
        for (const song in cd.song) {
          if (v_used_songs[cd.$.name + '/' + cd.song[song].$.track]) {
            cd.song[song].$.used = v_used_songs[cd.$.name + '/' + cd.song[song].$.track].length
            cd.song[song].$.used_mixes = v_used_songs[cd.$.name + '/' + cd.song[song].$.track]
          } else {
            cd.song[song].$.used = 0
          }
        }
      }

      for (const cd of v_cd_list.cds.cd) {
        for (const song of cd.song) {
          if (v_used_songs[cd.$.name + '/' + song.$.track]) {
            song.$.used_mixes = v_used_songs[cd.$.name + '/' + song.$.track]
          }
        }
      }
      // console.log('used', v_used_songs)

      deferred.resolve()
    })

  return deferred.promise
}

const check_dir_and_files = function () {
  console.log('[INFO] inizio check_dir_and_files')
  const deferred = q.defer()

  fs.open('./public/' + g_user, 'r', function (err, data) {
    if (err) {
      console.log('[ERROR] errore nel check directory')
      fs.mkdir('./public/' + g_user, { recursive: false }, function (err) {
        if (err) {
          console.log('[ERROR] errore nel create directory', err)
          deferred.reject()
        }

        fs.writeFile('./public/' + g_user + '/cd_list.xml', '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><cds/>', function (err) {
          if (err) deferred.reject()
          console.log('[INFO] file cd creato da 0!')

          fs.writeFile('./public/' + g_user + '/mix_list.xml', '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><mixes/>', function (err) {
            if (err) deferred.reject()
            console.log('[INFO] file mix creato da 0!')
            deferred.resolve()
          })
        })
      })
    }

    if (data) deferred.resolve()
    else {
      console.log('[ERROR] data non esiste, non so che vuol dire')
    }
  })

  return deferred.promise
}

exports.load = function (req, res, next, user) {
  // console.log('inizializzazione xml_data_reader per ' + user)
  g_user = user

  const set_variabili = function () {
    // var deferred = q.defer()

    req.xml_cd_list = xml_cd_list
    req.xml_mix_list = xml_mix_list
    req.v_cd_list = v_cd_list
    req.v_mix_list = v_mix_list
    req.v_used_songs = v_used_songs
    req.g_user = user
    console.log('[INFO] caricate le variabili in request')
    // deferred.resolve()

    // return deferred.promise
  }
  /* try{
  console.log(!req.v_cd_list, !req.v_mix_list, !req.v_cd_list.cds, !req.v_mix_list.mixes)}
  catch(e){console.log(!req.v_cd_list, !req.v_mix_list)} */
  if (!req.v_cd_list || !req.v_mix_list || !req.v_cd_list.cds || !req.v_mix_list.mixes) {
    check_dir_and_files()
      .then(carica_mix)
      .then(carica_cd)
      .then(set_variabili)
      .then(next)
  } else next()
}

exports.set_user_data = function (req, user, next) {
  console.log('[INFO] set user data', req.v_mix_list)
  g_user = user

  const set_variabili = function () {
    const deferred = q.defer()

    req.xml_cd_list = xml_cd_list
    req.xml_mix_list = xml_mix_list
    req.v_cd_list = v_cd_list
    req.v_mix_list = v_mix_list
    req.v_used_songs = v_used_songs
    req.g_user = user
    console.log('[INFO] caricate le variabili in request')
    deferred.resolve()

    return deferred.promise
  }

  if (!req.v_cd_list || !req.v_mix_list || !req.v_cd_list.cds || !req.v_mix_list.mixes) {
    carica_mix()
      .then(carica_cd)
      .then(set_variabili)
      .then(next)
  }
}
