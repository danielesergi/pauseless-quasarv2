const gulp = require('gulp')
const mocha = require('gulp-mocha')

function test () {
  return gulp.src(['test/mocha/1*/*.spec.js', 'test/mocha/2*/*.spec.js', 'test/mocha/3*/*.spec.js'])
    .pipe(mocha())
}

function singleTest () {
  // console.log(gulp.src(['test/mocha/1*/*.spec.js']))
  return gulp.src(['test/mocha/2*/cd.spec.js'])
    .pipe(mocha())
}

gulp.task('single', singleTest)

// Run our tests as the default task
gulp.task('default', test)
